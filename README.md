# m1if13-2020


**Table of contents**

[[_TOC_]]

## Information

### Server Access

We have 5 new accessible links :

* [Public](http://192.168.75.35/public/) (http://192.168.75.35/)
* [Admin](http://192.168.75.35/admin/) (http://192.168.75.35/admin/)  (**NOTE: login: `admin` and password `lyon1`**)
* [Public](http://192.168.75.35/public/) (http://192.168.75.35/public/)
* [Api](http://192.168.75.35/api/) (http://192.168.75.35/api/)
* [Assembly](http://192.168.75.35/assembly/) (http://192.168.75.35/assembly/)

### Client

The client files are in the [client files](app/client).

Launch locally
```shell script
webpack-dev-server --inline --hot
```

Build project
```shell script
webpack --config webpack.config.js
```

### Express server

The express server files are in the [server files](app/server).

Launch epxress server
```shell script
node app.js
```

### Admin

The admin files are in the [server files](app/server).

Launch locally
```shell script
webpack-dev-server --inline --hot
```

Build project
```shell script
webpack --config webpack.config.js
```

### Spring server

Run spring boot server
```shell script
mvn spring-boot:run
```

unitary test
```shell script
mvn test 
```

install project
```shell script
mvn install
```

## [Tp1](%1) & [Tp2](%2)

Link to the first lab [here](https://forge.univ-lyon1.fr/LIONEL.MEDINI/m1if13-2020/tree/master/tp1).
Link to the second lab [here](https://forge.univ-lyon1.fr/LIONEL.MEDINI/m1if13-2020/tree/master/tp2).


### Dev
#### Controllers

We have 2 controllers:
-   [OperationController](users/src/main/java/fr/univlyon1/m1if/m1if13/usersspringboot/controller/OperationController.java) :
The **OperationController** cover the authentification to the app with the usual methode (Login/Password).

-   [UserController](users/src/main/java/fr/univlyon1/m1if/m1if13/usersspringboot/controller/UserController.java):
The **UserController** allow some transaction from the app to the base. 

-   [AdminController](users/src/main/java/fr/univlyon1/m1if/m1if13/usersspringboot/controller/AdminController.java):
The **AdminController** allow login to administrator

#### DAO
- [UserDAO](users/src/main/java/fr/univlyon1/m1if/m1if13/usersspringboot/DAO/UserDao.java)

#### DTO
- [UserDTO](users/src/main/java/fr/univlyon1/m1if/m1if13/usersspringboot/DTO/UserDto.java)

### CI/CD

We had setup a gitlab-ci that runs tests and deploy on master branch automatically.

![CI/CD picture](doc/ci-cd.png)

#### API 
For the api, we managed for every html error codes needed. In order to do that we implemented MyErrorController.java which handle the errors.

To handle the CORS, we used Spring annotation. 

The api is documented with OpenAPI Swagger.

You can access to the documentation with this [link](http://192.168.75.35:8080/swagger/swagger-ui/index.html?url=/swagger/api-docs&validatorUrl=).


For the .yaml, you can access with this [link](http://192.168.75.35:8080/swagger/api-docs.yaml) or directly in the git repository. 

**NOTE : If you only request https://192.168.75.35/users , it is normal to get an error because you request nothing !**

#### List of available requests

##### users
**POST**"/users" -> create user<br>
**PUT** "/user/**login**" -> update user<br>
**DELETE** "/user/**login**" -> remove user<br>
**GET** "/users" -> user list<br>
**GET** "/user/**login**" -> get user<br>

##### login/logout
**POST** /login<br>
**DELETE** /logout<br>
**GET** /authenticate -> check user is connected<br>
**GET** /validUserToken -> check a token belongs to a user ( since TP5 )<br>

##### admin ( since TP4 )
**POST** /adminToken<br> token for admin 
**DELETE** /validAdminToken<br> check a token belongs to an admin<br>


## [TP3](%3)


### Client

We used Webpack to create a client bundle and put it in the server folder before the deployment.

### API

We used Express/Node to create the api of the game to access the resources.
 
- /resources
- /resources/{id}/image (**Never used**)
- /resources/{id}/position

Theses are the 3 requests you can do on the api.

Since [TP5](%5) we have implemented several other requests

- /resources/{id}/trophies
- /games
- /games/{id}
- /games/{id}/markers
- /games/{id}/marker/:marker_id
- /games/{id}/polygon
- /games/{id}/start
- /games/{id}/stop
- /games/{id}/join
- /games/{id}/leave/:player_id
- /games/{id}/subscription ( since [TP6](%6))


## [TP4](%4)

### State Management Pattern

We added `Vuex` to the project.

We implemented a [store](app/client/stores/client.js) including:
- a state
- mutations
- actions
- getters

We can then save:
- player login
- a true boolean if the player is connected
- current game
- trophies

At the launch of the site, we launch an action which updates the data in real time


### Router

several route are added with `view-router` ( [see here](app/client/js/main.js) ):
- /#/ -> root
- /#/login
- /#/trophies
- /#/win
- /#/lose

### App improvements added

- [x] login (with Spring)
- [ ] send an URL of an image
- [x] send its location to the server
- [x] update other players' data in real time
- [x] display local user trophy's list


## [TP5](%5)

### App

Webpack generates the files to deploy ( [see here](app/client/webpack.config.js) )

### App shell

We implemented a shell app which can be cached with our [service-worker.js](app/client/service-worker.js)

### Sensors and geolocation

We use the `watchPosition` function to update user's position on the server.

### Synchronize client and server

Added at [TP4](%4)

### Victory / defeat view

Pages added but there is a bug on the detection of the winner and loser

## [TP6](%6)

### Web App Manifest

[Manifest](app/client/manifest.json) added
- application logo
- application name
- worker scope

### Notifications

We used Firebase to notificate clients from the server.<br>

**NOTE: We use a proxy for notifications**
```javascript
webPush.sendNotification(this.subscription, payload, {
  proxy: "http://proxy.univ-lyon1.fr:3128"  // my proxy server
})
```

### AppShell caching

Caching all static elements, and use of `chunks` and `minimization` to optimize performance.
The application is available offline

![picutre](doc/audits.PNG)

### Advanced caching

Using `IndexedDB` to save `leaflet` tiles

![picutre](doc/indexedDB.PNG)

## [TP7](%7)

**NOTE: will be available Friday**
