var baseUrl = "https://192.168.75.35/users/";


var displayTestResult = function(fail, content) {
  var result = $("#result");
  result.css("color",fail ? "red" : "green");
  result.text(fail ? "FAIL" : "PASSED");
  $("#content").text(content);
};

var login = function() {

  var login = $("#login").val();
  var password = $("#password").val();


  $.ajax({
    url: baseUrl + "login" + "?login=" + login + "&password=" + password,
    type: "POST",
    crossDomain: true,
    dataType: "json",
    success: function(output, status, xhr) {
      var token = xhr.getResponseHeader("Authorization");
      localStorage.setItem("login",login);
      sessionStorage.setItem("token",token);
      $("#token").val(token);
      $("#connected").text("true");
      $("#location").html("<a href='" + baseUrl + "user/" + login + "'>" + login + "</a>");
    },
    error: function(err){
      displayTestResult(true,"Statut:" + err.status);
    }
  });
};

var logout = function() {

  var token = $("#token").val();

  $.ajax({
    url: baseUrl + "logout" + "?token=" + token,
    type: "DELETE",
    crossDomain: true,
    dataType: "json",
    success: function(output, status, xhr) {
      displayTestResult( false, "Statut:" + status)
      $("#connected").text("true");
    },
    error: function(err){
      displayTestResult(true,"Statut:" + err.status);
    }
  });

  sessionStorage.removeItem("token");

};

var authenticate = function() {

  var token = $("#token").val();
  var origin = $("#origin").val();

  $.ajax({
    url: baseUrl + "authenticate" + "?token=" + token + "&Origin=" + origin,
    type: "GET",
    crossDomain: true,
    dataType: "json",
    success: function (output, status, xhr) {
      displayTestResult( false, "Statut:" + status)
      $("#connected").text("true");
    },
    error: function (err) {
      displayTestResult(true,"Statut:" + err.status);
    }
  });
};


var getUser = function () {

  var login = $("#login").val();

  $.ajax({
    url: baseUrl + "user/" + login,
    type: "GET",
    crossDomain: true,
    dataType: "json",
    success: function (output, status, xhr) {
      displayTestResult(false,JSON.stringify(output));
    },
    error: function (err) {
      displayTestResult(true,"Statut:" + err.status);
    }
  });
};
