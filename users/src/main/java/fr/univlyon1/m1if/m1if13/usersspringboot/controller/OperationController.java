package fr.univlyon1.m1if.m1if13.usersspringboot.controller;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.interfaces.DecodedJWT;
import fr.univlyon1.m1if.m1if13.usersspringboot.DAO.UserDao;
import fr.univlyon1.m1if.m1if13.usersspringboot.model.User;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.headers.Header;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.naming.AuthenticationException;
import java.util.Optional;

import static com.auth0.jwt.impl.PublicClaims.ALGORITHM;
import static com.auth0.jwt.impl.PublicClaims.ISSUER;

@RestController
@CrossOrigin(
        origins = {
                "http://localhost:8080",
                "http://localhost",
                "http://192.168.75.35:8080",
                "https://192.168.75.35",
                "http://localhost:3376",
        },
        exposedHeaders = {
                "Authorization",
                "Location"
        })
public class OperationController {

    @Autowired
    private UserDao userDao;

    /**
     * Procédure de login "simple" d'un utilisateur
     * @param login Le login de l'utilisateur. L'utilisateur doit avoir été créé préalablement et son login doit être présent dans le DAO.
     * @param password Le password à vérifier.
     * @return Une ResponseEntity avec le JWT dans le header "Authentication" si le login s'est bien passé, et le code de statut approprié (204, 401 ou 404).
     */
    @PostMapping(
            path = "/login"
    )
    @Operation(summary = "User login",
            parameters = {
                    @Parameter(in = ParameterIn.QUERY, name = "login", description = "User login"),
                    @Parameter(in = ParameterIn.QUERY, name = "password", description = "User password"),
                    @Parameter(in = ParameterIn.HEADER, name = "Origin", description = "Origin request"),
            },
            responses = {
                    @ApiResponse(responseCode = "204", description = "Connection Success", headers = @Header(
                            name = "Authorization", description = "token"
                    )),
                    @ApiResponse(responseCode = "400", description = "Connection Failure",
                            content = {
                                    @Content(mediaType = "application/json"),
                                    @Content(mediaType = "application/xml")
                            }
                    ),
                    @ApiResponse(responseCode = "401", description = "Connection Failure",
                            content = {
                                    @Content(mediaType = "application/json"),
                                    @Content(mediaType = "application/xml")
                            }
                    ),
                    @ApiResponse(responseCode = "404", description = "Connection Failure",
                            content = {
                                    @Content(mediaType = "application/json"),
                                    @Content(mediaType = "application/xml")
                            }
                    )
            }
    )
    public ResponseEntity<String[]> login(
            @RequestParam("login") String login,
            @RequestParam("password") String password,
            @RequestHeader("Origin") String origin) {

        Optional<User> optionalUser = userDao.get(login);

        if (optionalUser.isEmpty()) {
            return new ResponseEntity<>(new String[] {
                    "Connection Failure",
                    "Username or password is incorrect"
            }, HttpStatus.NOT_FOUND);
        } else {
            User user = optionalUser.get();

            try {
                user.authenticate(password);

                JWTCreator.Builder builderToken = JWT.create();
                builderToken.withIssuer(ISSUER)
                        .withClaim("login", login)
                        .withClaim("Origin", origin);
                String token =  builderToken.sign(Algorithm.HMAC256(ALGORITHM));

                HttpHeaders headers = new HttpHeaders();
                headers.add("Authorization", token);


                return new ResponseEntity<>(headers, HttpStatus.NO_CONTENT);

            } catch (AuthenticationException e) {
                return new ResponseEntity<>(new String[] {
                        "Connection Failure",
                        "Username or password is incorrect"
                }, HttpStatus.UNAUTHORIZED);
            }
        }
    }

    /**
     * Réalise la déconnexion
     */
    @Operation(summary = "Logout",
            parameters = {
                    @Parameter(in = ParameterIn.QUERY, name = "token", description = "JWT Token"),
                    @Parameter(in = ParameterIn.HEADER, name = "Origin", description = "Origin request"),
            },
            responses = {
                    @ApiResponse(responseCode = "204", description = "Logout Success"),
                    @ApiResponse(responseCode = "401", description = "WoooooW ! This is a Bad Request !",
                            content = {
                                    @Content(mediaType = "application/json"),
                                    @Content(mediaType = "application/xml")
                            }
                    ),
            }
    )
    @DeleteMapping("/logout")
    public ResponseEntity<String[]> logout(
            @RequestParam(value = "token", required = false, defaultValue = "" ) String token,
            @RequestHeader(value = "Origin") String origin) {

        if (token.isEmpty()) {
            return new ResponseEntity<>(new String[]{
                    "Not Authorized.",
                    "You don't have the permission to do that."
            }, HttpStatus.UNAUTHORIZED);
        }

        try {
            DecodedJWT decodedJWT = JWT.decode(token);
            Optional<User> optionalUser = userDao.get(decodedJWT.getClaim("login").asString());
            if (optionalUser.isEmpty() || !decodedJWT.getClaim("Origin").asString().equals(origin)) {
                return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
            } else {
                User user = optionalUser.get();
                if (user.isConnected()) {
                    user.disconnect();
                } else {
                    return new ResponseEntity<>(new String[]{
                            "WoooooW ! This is a Bad Request !",
                            "You aren't connected !"
                    }, HttpStatus.UNAUTHORIZED);
                }
            }
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);

        } catch (JWTDecodeException ex) {
            return new ResponseEntity<>(new String[]{
                    "Not Authorized.",
                    "You don't have the permission to do that."
            }, HttpStatus.UNAUTHORIZED);
        }
    }

    /**
     * Méthode destinée au serveur Node pour valider l'authentification d'un utilisateur.
     * @param token Le token JWT qui se trouve dans le header "Authentication" de la requête
     * @param origin L'origine de la requête (pour la comparer avec celle du client, stockée dans le token JWT)
     * @return Une réponse vide avec un code de statut approprié (204, 400, 401).
     */
    @Operation(summary = "Authenticate",
            parameters = {
                    @Parameter(in = ParameterIn.QUERY, name = "token", description = "JWT Token"),
                    @Parameter(in = ParameterIn.QUERY, name = "Origin", description = "Origin request"),
            },
            responses = {
                    @ApiResponse(responseCode = "204", description = "Authenticate Success"),
                    @ApiResponse(responseCode = "400", description = "WoooooW ! This is a Bad Request !",
                            content = {
                                    @Content(mediaType = "application/json"),
                                    @Content(mediaType = "application/xml")
                            }
                    ),
                    @ApiResponse(responseCode = "401", description = "Not Authorized.",
                            content = {
                                    @Content(mediaType = "application/json"),
                                    @Content(mediaType = "application/xml")
                            }
                    ),
            }
    )
    @GetMapping("/authenticate")
    public ResponseEntity<String[]> authenticate(
            @RequestParam(value = "token", required = false, defaultValue = "" ) String token,
            @RequestParam(value = "Origin") String origin) {

        if (token.isEmpty()) {
            return new ResponseEntity<>(new String[]{
                    "Not Authorized.",
                    "You don't have the permission to do that."
            }, HttpStatus.UNAUTHORIZED);
        }

        try {
            DecodedJWT decodedJWT = JWT.decode(token);

            Optional<User> optionalUser = userDao.get(decodedJWT.getClaim("login").asString());

            if (optionalUser.isEmpty() || !decodedJWT.getClaim("Origin").asString().equals(origin)) {
                return new ResponseEntity<>(new String[]{
                        "Not Authorized.",
                        "You don't have the permission to do that."
                }, HttpStatus.UNAUTHORIZED);
            } else {

                User user = optionalUser.get();

                if (user.isConnected()) {
                    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
                } else {
                    return new ResponseEntity<>(new String[]{
                            "WoooooW ! This is a Bad Request !",
                            "You aren't connected !"
                    }, HttpStatus.BAD_REQUEST);
                }
            }
        } catch (JWTDecodeException ex) {
            return new ResponseEntity<>(new String[]{
                    "Not Authorized.",
                    "You don't have the permission to do that."
            }, HttpStatus.UNAUTHORIZED);
        }
    }

    /**
     * Méthode destinée à vérifier que le token correspond bien au token de l'utilisateur comparé.
     * @param token Le token JWT qui se trouve dans le header "Authentication" de la requête
     * @param login Login de l'utilisateur de la requête (pour la comparer avec celle du client, stockée dans le token JWT)
     * @return Une réponse vide avec un code de statut approprié (204, 400, 401).
     */
    @Operation(summary = "validUserToken",
            parameters = {
                    @Parameter(in = ParameterIn.QUERY, name = "token", description = "JWT Token"),
                    @Parameter(in = ParameterIn.QUERY, name = "login", description = "User login"),
            },
            responses = {
                    @ApiResponse(responseCode = "204", description = "Correct login"),
                    @ApiResponse(responseCode = "400", description = "WoooooW ! This is a Bad Request !",
                            content = {
                                    @Content(mediaType = "application/json"),
                                    @Content(mediaType = "application/xml")
                            }
                    ),
                    @ApiResponse(responseCode = "401", description = "Not Authorized.",
                            content = {
                                    @Content(mediaType = "application/json"),
                                    @Content(mediaType = "application/xml")
                            }
                    ),
            }
    )
    @GetMapping("/validUserToken")
    public ResponseEntity<String[]> validUserToken(
            @RequestParam(value = "token", required = false, defaultValue = "" ) String token,
            @RequestParam(value = "login") String login) {

        if (token.isEmpty()) {
            return new ResponseEntity<>(new String[]{
                    "Not Authorized.",
                    "You don't have the permission to do that."
            }, HttpStatus.UNAUTHORIZED);
        }

        try {
            DecodedJWT decodedJWT = JWT.decode(token);

            Optional<User> optionalUser = userDao.get(decodedJWT.getClaim("login").asString());

            if (optionalUser.isEmpty() || !decodedJWT.getClaim("login").asString().equals(login)) {
                return new ResponseEntity<>(new String[]{
                        "Not Authorized.",
                        "You don't have the permission to do that."
                }, HttpStatus.UNAUTHORIZED);
            } else {

                User user = optionalUser.get();

                if (user.isConnected()) {
                    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
                } else {
                    return new ResponseEntity<>(new String[]{
                            "WoooooW ! This is a Bad Request !",
                            "You aren't connected !"
                    }, HttpStatus.BAD_REQUEST);
                }
            }
        } catch (JWTDecodeException ex) {
            return new ResponseEntity<>(new String[]{
                    "Not Authorized.",
                    "You don't have the permission to do that."
            }, HttpStatus.UNAUTHORIZED);
        }
    }


}