package fr.univlyon1.m1if.m1if13.usersspringboot.DTO;

import fr.univlyon1.m1if.m1if13.usersspringboot.model.User;

/**
 * DTO de l'objet User
 */
public class UserDto {

    private String login;

    private String password;

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public User toUser() {
        return new User(login,password);
    }
}