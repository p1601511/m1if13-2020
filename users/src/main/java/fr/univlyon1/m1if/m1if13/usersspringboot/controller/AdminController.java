package fr.univlyon1.m1if.m1if13.usersspringboot.controller;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.interfaces.DecodedJWT;
import fr.univlyon1.m1if.m1if13.usersspringboot.DAO.UserDao;
import fr.univlyon1.m1if.m1if13.usersspringboot.model.User;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.headers.Header;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.naming.AuthenticationException;
import java.util.Optional;

import static com.auth0.jwt.impl.PublicClaims.ALGORITHM;
import static com.auth0.jwt.impl.PublicClaims.ISSUER;

@RestController
@CrossOrigin(
        origins = {
                "http://localhost:8080",
                "http://localhost",
                "http://192.168.75.35:8080",
                "https://192.168.75.35",
                "http://localhost:3376",
        },
        exposedHeaders = {
                "Authorization",
                "Location"
        })
public class AdminController {

    //TODO ceci n'est pas bien du tout mais normalement personne n'a accès au code

    private String login = "admin";

    private String password = "lyon1";

    /**
     * Procédure de login "simple" d'un administrateur
     * @param login Le login de l'administrateur
     * @param password Le password à vérifier.
     * @return Une ResponseEntity avec le JWT dans le header "Authentication" si le login s'est bien passé, et le code de statut approprié (204, 401 ou 404).
     */
    @PostMapping(
            path = "/adminToken"
    )
    @Operation(summary = "Admin Token",
            parameters = {
                    @Parameter(in = ParameterIn.QUERY, name = "login", description = "Admin login"),
                    @Parameter(in = ParameterIn.QUERY, name = "password", description = "Admin password"),
                    @Parameter(in = ParameterIn.HEADER, name = "Origin", description = "Origin request"),
            },
            responses = {
                    @ApiResponse(responseCode = "204", description = "Connection Success", headers = @Header(
                            name = "Authorization", description = "token"
                    )),
                    @ApiResponse(responseCode = "400", description = "Connection Failure",
                            content = {
                                    @Content(mediaType = "application/json"),
                                    @Content(mediaType = "application/xml")
                            }
                    ),
                    @ApiResponse(responseCode = "401", description = "Connection Failure",
                            content = {
                                    @Content(mediaType = "application/json"),
                                    @Content(mediaType = "application/xml")
                            }
                    ),
                    @ApiResponse(responseCode = "404", description = "Connection Failure",
                            content = {
                                    @Content(mediaType = "application/json"),
                                    @Content(mediaType = "application/xml")
                            }
                    )
            }
    )
    public ResponseEntity<String[]> adminToken(
            @RequestParam("login") String login,
            @RequestParam("password") String password,
            @RequestHeader("Origin") String origin) {

        if (this.login.equals(login) && this.password.equals(password)) {

            JWTCreator.Builder builderToken = JWT.create();
            builderToken.withIssuer(ISSUER)
                    .withClaim("login", login)
                    .withClaim("Origin", origin);
            String token =  builderToken.sign(Algorithm.HMAC256(ALGORITHM));

            HttpHeaders headers = new HttpHeaders();
            headers.add("Authorization", token);


            return new ResponseEntity<>(headers, HttpStatus.NO_CONTENT);

        } else {
            return new ResponseEntity<>(new String[] {
                    "Connection Failure",
                    "Username or password is incorrect"
            }, HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Méthode destinée à vérifier que le token correspond bien au token de l'administrateur comparé.
     * @param token Le token JWT qui se trouve dans le header "Authentication" de la requête
     * @param origin Login de l'administrateur de la requête (pour la comparer avec celle du client, stockée dans le token JWT)
     * @return Une réponse vide avec un code de statut approprié (204, 400, 401).
     */
    @Operation(summary = "validUserToken",
            parameters = {
                    @Parameter(in = ParameterIn.QUERY, name = "token", description = "JWT Token"),
                    @Parameter(in = ParameterIn.QUERY, name = "origin", description = "origin"),
            },
            responses = {
                    @ApiResponse(responseCode = "204", description = "Correct login"),
                    @ApiResponse(responseCode = "400", description = "WoooooW ! This is a Bad Request !",
                            content = {
                                    @Content(mediaType = "application/json"),
                                    @Content(mediaType = "application/xml")
                            }
                    ),
                    @ApiResponse(responseCode = "401", description = "Not Authorized.",
                            content = {
                                    @Content(mediaType = "application/json"),
                                    @Content(mediaType = "application/xml")
                            }
                    ),
            }
    )
    @GetMapping("/validAdminToken")
    public ResponseEntity<String[]> validUserToken(
            @RequestParam(value = "token", required = false, defaultValue = "" ) String token,
            @RequestParam(value = "origin") String origin) {

        if (origin.isEmpty()) {
            return new ResponseEntity<>(new String[]{
                    "Not Authorized.",
                    "You don't have the permission to do that."
            }, HttpStatus.UNAUTHORIZED);
        }

        try {
            DecodedJWT decodedJWT = JWT.decode(token);

            if (!decodedJWT.getClaim("Origin").asString().equals(origin)) {
                return new ResponseEntity<>(new String[]{
                        "Not Authorized.",
                        "You don't have the permission to do that."
                }, HttpStatus.UNAUTHORIZED);
            } else {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
        } catch (JWTDecodeException ex) {
            return new ResponseEntity<>(new String[]{
                    "Not Authorized.",
                    "You don't have the permission to do that."
            }, HttpStatus.UNAUTHORIZED);
        }
    }


}