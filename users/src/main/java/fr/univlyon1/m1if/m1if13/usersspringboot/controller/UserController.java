package fr.univlyon1.m1if.m1if13.usersspringboot.controller;

import fr.univlyon1.m1if.m1if13.usersspringboot.DAO.UserDao;
import fr.univlyon1.m1if.m1if13.usersspringboot.DTO.UserDto;
import fr.univlyon1.m1if.m1if13.usersspringboot.model.User;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

@RestController("user")
@RequestMapping("")
@CrossOrigin(
        origins = {
                "*",
        }
)
public class UserController {

    @Autowired
    private UserDao userDao;

    @GetMapping(
            path = "/users",
            produces = {
                    MediaType.TEXT_HTML_VALUE,
                    MediaType.APPLICATION_JSON_VALUE,
                    MediaType.APPLICATION_XML_VALUE
            }
    )
    @Operation(summary = "Get all users",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Logout Success",
                    content = {
                            @Content(mediaType = "application/json"),
                            @Content(mediaType = "application/xml")
                    }),
            }
    )
    public ModelAndView getUsers() {
        ModelAndView modelAndView = new ModelAndView("users.html");
        modelAndView.addObject("users", userDao.getAll());
        return modelAndView;
    }

    @GetMapping(
            path = "/user/{id}",
            produces = {
                    MediaType.APPLICATION_XML_VALUE,
                    MediaType.APPLICATION_JSON_VALUE
            }
    )
    @Operation(summary = "Get users with an Id in the path.",
            parameters = {
                    @Parameter(in = ParameterIn.QUERY, name = "User", description = "User ID"),
            },
            responses = {
                    @ApiResponse(responseCode = "200", description = "OK",
                            content = {
                                    @Content(mediaType = "application/json"),
                                    @Content(mediaType = "application/xml")
                            }),
                    @ApiResponse(responseCode = "400", description = "WoooooW ! This is a Bad Request !",
                            content = {
                                    @Content(mediaType = "application/json"),
                                    @Content(mediaType = "application/xml")
                            }
                    ),
            }
    )
    public ResponseEntity<User> getUser(@PathVariable String id, HttpServletResponse response) {
        Optional<User> optionalUser = userDao.get(id);
        if (optionalUser.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        User user = optionalUser.get();
        return new ResponseEntity<>(user, HttpStatus.OK);

    }



    @PostMapping(
            path = "/users",
            consumes = {
                    MediaType.APPLICATION_XML_VALUE,
                    MediaType.APPLICATION_JSON_VALUE
            }
    )
    @Operation(summary = "Create user",
            description = "This can only be done by the logged in user.",
            responses = {
                    @ApiResponse(responseCode = "201", description = "Created",
                            content = {
                                    @Content(mediaType = "application/json"),
                                    @Content(mediaType = "application/xml")
                            }),
                    @ApiResponse(responseCode = "400", description = "WoooooW ! This is a Bad Request !",
                            content = {
                                    @Content(mediaType = "application/json"),
                                    @Content(mediaType = "application/xml")
                            }
                    ),
                    @ApiResponse(responseCode = "401", description = "Methode not allowed ! Don't even go further !",
                            content = {
                                    @Content(mediaType = "application/json"),
                                    @Content(mediaType = "application/xml")
                            }
                    )
            }
    )
    public ResponseEntity<Void> addUser(@RequestBody UserDto userDto)  throws Exception {

        // On vérifie que le login est défini
        if (userDto.getLogin().isEmpty()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        // On vérifie que l'utlisateur n'existe pas
        if (userDao.get(userDto.getLogin()).isPresent()) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }

        // On ajoute l'utilisateur dans la base de données
        userDao.save(userDto.toUser());

        HttpHeaders headers = new HttpHeaders();
        headers.add("Location", "/user/" + userDto.getLogin());

        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    @PutMapping(
            path = "/user/{id}",
            consumes = {
                    MediaType.APPLICATION_XML_VALUE,
                    MediaType.APPLICATION_JSON_VALUE
            }
    )
    @Operation(summary = "Create or edit a user", description = "This can only be done by the logged in user.",
            parameters = {
                    @Parameter(in = ParameterIn.QUERY, name = "User", description = "User ID"),
            },
            responses = {
                    @ApiResponse(responseCode = "204", description = "Modified",
                            content = {
                                    @Content(mediaType = "application/json"),
                                    @Content(mediaType = "application/xml")
                            }),
                    @ApiResponse(responseCode = "400", description = "WoooooW ! This is a Bad Request !",
                            content = {
                                    @Content(mediaType = "application/json"),
                                    @Content(mediaType = "application/xml")
                            }
                    ),
                    @ApiResponse(responseCode = "401", description = "Methode not allowed ! Don't even go further !",
                            content = {
                                    @Content(mediaType = "application/json"),
                                    @Content(mediaType = "application/xml")
                            }
                    )
            }
    )
    public ResponseEntity<Void> putUser(@RequestBody UserDto userDto, @PathVariable String id) {

        Optional<User> optionalUser = userDao.get(id);

        // Si l'utilisateur n'existe pas UNAUTHORIZED
        if (optionalUser.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }

        User user = optionalUser.get();

        // si mot de passe vide BAD REQUEST
        if (userDto.getPassword().isEmpty()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        userDao.update(user,new String[]{
                userDto.getPassword()
        });
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Operation(summary = "Delete a user", description = "This can only be done by the logged in user.",
            parameters = {
                    @Parameter(in = ParameterIn.QUERY, name = "User", description = "User ID"),
            },
            responses = {
                    @ApiResponse(responseCode = "204", description = "Deleted",
                            content = {
                                    @Content(mediaType = "application/json"),
                                    @Content(mediaType = "application/xml")
                            }),
                    @ApiResponse(responseCode = "401", description = "Methode not allowed ! Don't even go further !",
                            content = {
                                    @Content(mediaType = "application/json"),
                                    @Content(mediaType = "application/xml")
                            }
                    )
            }
    )
    @DeleteMapping(path = "/user/{id}")
    public ResponseEntity<Void> deleteUser(@PathVariable String id) {

        Optional<User> optionalUser = userDao.get(id);

        // Si l'utilisateur n'existe pas UNAUTHORIZED
        if (optionalUser.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }

        userDao.delete(optionalUser.get());
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}