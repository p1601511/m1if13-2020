package fr.univlyon1.m1if.m1if13.usersspringboot.controller;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static org.springframework.http.HttpStatus.*;

@RestController
public class MyErrorController implements ErrorController {

    public static String[] getMessage(int status) {
        switch (HttpStatus.valueOf(status)) {
            case NOT_FOUND:
                return new String[] {
                        "Oops! Page not found",
                        "we are sorry, but the page you requested was not found."
                };
            case BAD_REQUEST:
                return new String[] {
                        "WoooooW ! This is a Bad Request !",
                        "Are you sure to do what you wanted to do ?"
                };
            case METHOD_NOT_ALLOWED:
                return new String[] {
                        "Methode not allowed !",
                        "Don't even go further !"
                };
            case UNAUTHORIZED:
                return new String[] {
                        "Not Authorized.",
                        "You don't have the permission to do that."
                };
            default:
                throw new IllegalStateException("Unexpected value: " + status);
        }
    }

    @RequestMapping(
            path = "/error",
            produces = {
                    MediaType.APPLICATION_JSON_VALUE,
                    MediaType.APPLICATION_XML_VALUE
            }
    )
    public ResponseEntity<String[]> handleErrorDefault(HttpServletResponse response) {
        int status = response.getStatus();
        return new ResponseEntity<>(getMessage(status), HttpStatus.valueOf(status));
    }


    @RequestMapping(
            path = "/error",
            produces = MediaType.TEXT_HTML_VALUE
    )
    public ModelAndView handleErrorHTML(HttpServletResponse response) {
        ModelAndView modelAndView = new ModelAndView("error.html");
        modelAndView.addObject("httpStatus", response.getStatus());

        String[] message = getMessage(response.getStatus());

        modelAndView.addObject("errorMessage1", message[0]);
        modelAndView.addObject("errorMessage2", message[1]);


        //do something like logging
        return modelAndView;
    }


    @Override
    public String getErrorPath() {
        return "/error";
    }
}