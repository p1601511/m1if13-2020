package fr.univlyon1.m1if.m1if13.usersspringboot.DAO;

import fr.univlyon1.m1if.m1if13.usersspringboot.model.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public class UserDao implements Dao<User> {

    //TODO temporaire avant base de données
    private static List<User> userList = new ArrayList<>();

    @Override
    public Optional<User> get(String id) {
        return userList.stream().filter(user -> user.getLogin().equals(id)).findFirst();
    }

    @Override
    public Set<String> getAll() {
        return userList.stream().map(User::getLogin).collect(Collectors.toSet());
    }

    @Override
    public void save(User user) {
        Optional<User> optionalUser = this.get(user.getLogin());
        if (optionalUser.isEmpty()) {
           userList.add(user);
        }
    }

    @Override
    public void update(User user, String[] params) {
        /**
         * String[] params
         * 0: password
         */

        Optional<User> optionalUser = get(user.getLogin());
        if (optionalUser.isPresent()) {
            User lastUser = optionalUser.get();
            user = lastUser;
            user.setPassword(params[0]);
        }
    }

    @Override
    public void delete(User user) {
        userList.remove(user);
    }
}