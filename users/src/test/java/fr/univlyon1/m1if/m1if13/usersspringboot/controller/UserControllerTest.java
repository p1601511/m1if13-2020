package fr.univlyon1.m1if.m1if13.usersspringboot.controller;

import fr.univlyon1.m1if.m1if13.usersspringboot.DAO.UserDao;
import fr.univlyon1.m1if.m1if13.usersspringboot.configuration.AppConfig;
import fr.univlyon1.m1if.m1if13.usersspringboot.model.User;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.io.File;
import java.nio.file.Files;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = { UserController.class, UserDao.class, AppConfig.class})
public class UserControllerTest {

    private MockMvc mvc;
    private ClassLoader classLoader;

    @Autowired
    private UserDao userDao;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Before
    public void setup(){
        if (userDao.get("12").isEmpty()) {
            userDao.save(new User("12", "1234"));
        }

        this.mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        this.classLoader = getClass().getClassLoader();

    }

    @Test
    public void getUsers() {
        try {
            mvc.perform(
                    MockMvcRequestBuilders.get("/users")
                            .accept(MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML, MediaType.TEXT_HTML))
                    //.andDo(print())
                    .andExpect(status().isOk());

        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }

    @Test
    public void getUserOk() {

        try {
           mvc.perform(
                MockMvcRequestBuilders.get("/user/{id}", "12")
                        .accept(MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML))
                    .andDo(print())
                    .andExpect(status().isOk());

        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }


    @Test
    public void getUserNotOk() {
        try {
            mvc.perform(
                    MockMvcRequestBuilders.get("/user/{id}", "13")
                            .contextPath("")
                            .accept(MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML))
                    .andDo(print())
                    .andExpect(status().isBadRequest());

        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }

    @Test
    public void addUser() {

        try {


            File file = new File(
                    classLoader.getResource("test/add_User_OK.json").getFile()
            );

            String content = Files.readAllLines(file.toPath())
                    .stream().map(Object::toString)
                    .collect(Collectors.joining());

            ResultActions resultActions = mvc.perform(
                    MockMvcRequestBuilders.post("/users")
                            .accept(MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML)
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(content))
                    .andDo(print())
                    .andExpect(status().isCreated()
            );

            MvcResult result = resultActions.andReturn();
            String location = result.getResponse().getHeader("Location");

            Assert.assertNotNull(location);

            mvc.perform(
                    MockMvcRequestBuilders.get(location)
                            .accept(MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML, MediaType.TEXT_HTML))
                    .andDo(print())
                    .andExpect(status().isOk());

        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }


    @Test
    public void addUserNotOk() {

        try {

            File file = new File(
                    classLoader.getResource("test/add_User_Not_OK.json").getFile()
            );

            String content = Files.readAllLines(file.toPath())
                    .stream().map(Object::toString)
                    .collect(Collectors.joining());

            ResultActions resultActions = mvc.perform(
                    MockMvcRequestBuilders.post("/users")
                            .accept(MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML)
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(content))
                    .andDo(print())
                    .andExpect(status().isUnauthorized()
            );

            MvcResult result = resultActions.andReturn();
            String location = result.getResponse().getHeader("Location");

            Assert.assertNull(location);

        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }

    @Test
    public void putUserNotOk() {
        try {
            File file = new File(
                    classLoader.getResource("test/put_User_Not_OK.json").getFile()
            );

            String content = Files.readAllLines(file.toPath())
                    .stream().map(Object::toString)
                    .collect(Collectors.joining());

            mvc.perform(
                    MockMvcRequestBuilders.put("/user/{id}", "14")
                            .accept(MediaType.APPLICATION_JSON)
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(content))
                    .andDo(print())
                    .andExpect(status().isUnauthorized()
                    );

        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }


    @Test
    public void putUserNoPass() {
        try {

            File file = new File(
                    classLoader.getResource("test/put_User_No_Pass.json").getFile()
            );

            String content = Files.readAllLines(file.toPath())
                    .stream().map(Object::toString)
                    .collect(Collectors.joining());

            mvc.perform(
                    MockMvcRequestBuilders.put("/user/{id}", "12")
                            .accept(MediaType.APPLICATION_JSON)
                            .contentType(MediaType.APPLICATION_JSON)
                            .content("put_User_No_Pass.json"))
                    .andDo(print())
                    .andExpect(status().isBadRequest()
                    );

        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }


    @Test
    public void putUser() {
        try {

            File file = new File(
                    classLoader.getResource("test/put_OK.json").getFile()
            );

            String content = Files.readAllLines(file.toPath())
                    .stream().map(Object::toString)
                    .collect(Collectors.joining());

            mvc.perform(
                    MockMvcRequestBuilders.put("/user/{id}", "12")
                            .accept(MediaType.APPLICATION_JSON)
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(content))
                    .andDo(print())
                    .andExpect(status().isNoContent()
                    );

            Optional<User> userOptional = userDao.get("12");
            Assert.assertNotNull(userOptional);
            Assert.assertTrue(userOptional.isPresent());

            User user = userOptional.get();

            user.authenticate("test");

        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }

    @Test
    public void deleteUser() {
        try {
            mvc.perform(
                    MockMvcRequestBuilders.delete("/user/{id}", "12")
                            .accept(MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML))
                    .andDo(print())
                    .andExpect(status().isNoContent()
                    );

        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }


    @Test
    public void deleteUserNotOk() {
        try {
            mvc.perform(
                    MockMvcRequestBuilders.delete("/user/13")
                            .contextPath("")
                            .accept(MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML))
                    .andDo(print())
                    .andExpect(status().isUnauthorized()
                    );

        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }
}
