package fr.univlyon1.m1if.m1if13.usersspringboot.controller;


import fr.univlyon1.m1if.m1if13.usersspringboot.DAO.UserDao;
import fr.univlyon1.m1if.m1if13.usersspringboot.configuration.AppConfig;
import fr.univlyon1.m1if.m1if13.usersspringboot.model.User;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.WebApplicationContext;

import java.io.File;
import java.nio.file.Files;
import java.util.stream.Collectors;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = { UserDao.class, AppConfig.class, OperationController.class})
public class OperationControllerTest {

    private MockMvc mvc;
    private ClassLoader classLoader;

    @Autowired
    private UserDao userDao;

    @Autowired
    private WebApplicationContext webApplicationContext;


    @Before
    public void setup(){
        if (userDao.get("test").isEmpty()) {
            userDao.save(new User("test", "test"));
        }

        this.mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        this.classLoader = getClass().getClassLoader();

    }


    @Test
    public void login() {
        try {

            mvc.perform(
                    MockMvcRequestBuilders.post("/login")
                            .param("login", "test")
                            .param("password", "test")
                            .header("Origin", "http://localhost")
                            )
                    .andDo(print())
                    .andExpect(status().isNoContent()

            );

        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }

    @Test
    public void loginNotOk() {
        try {

            mvc.perform(
                    MockMvcRequestBuilders.post("/login")
                            .param("login", "test")
                            .param("password", "test2")
                            .header("Origin", "http://localhost")
            )
                    .andDo(print())
                    .andExpect(status().isUnauthorized()
                    );

        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }

    @Test
    public void loginNotFound() {
        try {

            mvc.perform(
                    MockMvcRequestBuilders.post("/login")
                            .param("login", "test2")
                            .param("password", "test2")
                            .header("Origin", "http://localhost")
            )
                    .andDo(print())
                    .andExpect(status().isNotFound()
                    );

        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }



    @Test
    public void authenticate() {
        try {
            ResultActions resultActions = mvc.perform(
                    MockMvcRequestBuilders.post("/login")
                            .param("login", "test")
                            .param("password", "test")
                            .header("Origin", "http://localhost")
            )
                    .andDo(print())
                    .andExpect(status().isNoContent());

            MvcResult result = resultActions.andReturn();
            String token = result.getResponse().getHeader("Authorization");
            System.out.println(token);
            mvc.perform(
                    MockMvcRequestBuilders.get("/authenticate")
                            .param("token", token)
                            .param("Origin", "http://localhost"))
                    .andDo(print())
                    .andExpect(status().isNoContent());

        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }

    @Test
    public void authenticate_WithoutOrigin() {
        try {
            ResultActions resultActions = mvc.perform(
                    MockMvcRequestBuilders.post("/login")
                            .param("login", "test")
                            .param("password", "test")
            )
                    .andDo(print())
                    .andExpect(status().isBadRequest());

        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }

    @Test
    public void authenticate_BadOrigin() {
        try {
            ResultActions resultActions = mvc.perform(
                    MockMvcRequestBuilders.post("/login")
                            .param("login", "test")
                            .param("password", "test")
                            .header("Origin", "http://test.com")
            )
                    .andDo(print())
                    .andExpect(status().isForbidden());

        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }

    @Test
    public void authenticateNotConnected() {
        try {
            ResultActions resultActions = mvc.perform(
                    MockMvcRequestBuilders.post("/login")
                            .param("login", "test")
                            .param("password", "test")
                            .header("Origin", "http://localhost")
            )
                    .andDo(print())
                    .andExpect(status().isNoContent());

            MvcResult result = resultActions.andReturn();
            String token = result.getResponse().getHeader("Authorization");
            System.out.println(token);

            mvc.perform(
                    MockMvcRequestBuilders.delete("/logout")
                            .param("token", token)
                            .header("Origin", "http://localhost"))
                    .andDo(print())
                    .andExpect(status().isNoContent());


            mvc.perform(
                    MockMvcRequestBuilders.get("/authenticate")
                            .param("token", token)
                            .header("Origin", "http://localhost"))
                    .andDo(print())
                    .andExpect(status().isBadRequest());

        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }

    @Test
    public void authenticateNotOK() {
        try {
            ResultActions resultActions = mvc.perform(
                    MockMvcRequestBuilders.post("/login")
                            .param("login", "test2")
                            .param("password", "test")
                            .header("Origin", "http://localhost")
            )
                    .andDo(print())
                    .andExpect(status().isNotFound());

            MvcResult result = resultActions.andReturn();
            String token = result.getResponse().getHeader("Authorization");
            System.out.println(token);
            mvc.perform(
                    MockMvcRequestBuilders.get("/authenticate")
                            .param("token", token)
                            .param("Origin", "http://localhost:8080"))
                    .andDo(print())
                    .andExpect(status().isUnauthorized());

        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }

    @Test
    public void logout() {
        try {
            ResultActions resultActions = mvc.perform(
                    MockMvcRequestBuilders.post("/login")
                            .param("login", "test")
                            .param("password", "test")
                            .header("Origin", "http://localhost")
            )
                    .andDo(print())
                    .andExpect(status().isNoContent());

            MvcResult result = resultActions.andReturn();
            String token = result.getResponse().getHeader("Authorization");
            System.out.println(token);
            mvc.perform(
                    MockMvcRequestBuilders.delete("/logout")
                            .param("token", token)
                            .header("Origin", "http://localhost"))
                    .andDo(print())
                    .andExpect(status().isNoContent());

        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }

    @Test
    public void logoutNotOk() {
        try {
            ResultActions resultActions = mvc.perform(
                    MockMvcRequestBuilders.post("/login")
                            .param("login", "test")
                            .param("password", "test")
                            .header("Origin", "http://localhost")
            )
                    .andDo(print())
                    .andExpect(status().isNoContent());

            MvcResult result = resultActions.andReturn();
            String token = result.getResponse().getHeader("Authorization");
            System.out.println(token);

            mvc.perform(
                    MockMvcRequestBuilders.delete("/logout")
                            .param("token", token)
                            .header("Origin", "http://localhost"))
                    .andDo(print())
                    .andExpect(status().isNoContent());


            mvc.perform(
                    MockMvcRequestBuilders.delete("/logout")
                            .param("token", token))
                    .andDo(print())
                    .andExpect(status().isBadRequest());

        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }

    @Test
    public void logoutNotFound() {
        try {
            ResultActions resultActions = mvc.perform(
                    MockMvcRequestBuilders.post("/login")
                            .param("login", "tesst2")
                            .param("password", "test")
                            .header("Origin", "http://localhost")
            )
                    .andDo(print())
                    .andExpect(status().isNotFound());

            MvcResult result = resultActions.andReturn();
            String token = result.getResponse().getHeader("Authorization");
            System.out.println(token);
            mvc.perform(
                    MockMvcRequestBuilders.delete("/logout")
                            .param("token", token)
                            .header("Origin", "http://localhost"))
                    .andDo(print())
                    .andExpect(status().isUnauthorized());

        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }

    @Test
    public void validUserTokenOk() {
        try {

            ResultActions resultActions = mvc.perform(
                    MockMvcRequestBuilders.post("/login")
                            .param("login", "test")
                            .param("password", "test")
                            .header("Origin", "http://localhost")
            )
                    .andDo(print())
                    .andExpect(status().isNoContent()

                    );

            MvcResult result = resultActions.andReturn();
            String token = result.getResponse().getHeader("Authorization");


            mvc.perform(
                    MockMvcRequestBuilders.get("/validUserToken")
                            .param("login", "test")
                            .param("token", token)
            )
                    .andDo(print())
                    .andExpect(status().isNoContent()

                    );

        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }

    @Test
    public void validUserTokenNotOk() {
        try {

            ResultActions resultActions = mvc.perform(
                    MockMvcRequestBuilders.post("/login")
                            .param("login", "test")
                            .param("password", "test")
                            .header("Origin", "http://localhost")
            )
                    .andDo(print())
                    .andExpect(status().isNoContent()

                    );

            MvcResult result = resultActions.andReturn();
            String token = "bad token";


            mvc.perform(
                    MockMvcRequestBuilders.get("/validUserToken")
                            .param("login", "test")
                            .param("token", token)
            )
                    .andDo(print())
                    .andExpect(status().isUnauthorized()

                    );



        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }
}