package fr.univlyon1.m1if.m1if13.usersspringboot.DAO;

import fr.univlyon1.m1if.m1if13.usersspringboot.model.User;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.naming.AuthenticationException;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

public class UserDaoTest {

    UserDao userDaoExpected = new UserDao();

    @Before
    public void starter() {
        userDaoExpected.save(new User("test1", "test1"));
        userDaoExpected.save(new User("test2", "test2"));
        userDaoExpected.save(new User("test3", "test3"));
    }

    @Test
    public void get() {
        User user = new User("test1", "test1");
        User userexpected = userDaoExpected.get("test1").get();
        Assert.assertEquals(user.getLogin(), userexpected.getLogin());
    }

    @Test
    public void getAll() {
        Assert.assertNotNull(userDaoExpected.getAll());
    }

    @Test
    public void save() {
        User user = new User("lala", "lala");
        userDaoExpected.save(user);
        Assert.assertTrue(userDaoExpected.get("lala").isPresent());
    }

    @Test
    public void update() {
        User user = new User("test1", "test1");
        userDaoExpected.update(user, new String[] {
                "lala0"
        });
        try {

            Optional<User> newUser = userDaoExpected.get(user.getLogin());
            Assert.assertTrue(newUser.isPresent());

            newUser.get().authenticate("lala0");

        } catch (AuthenticationException e) {
            e.printStackTrace();
            Assert.fail();
        }
    }

    @Test
    public void delete() {
        User user = userDaoExpected.get("test1").get();
        userDaoExpected.delete(user);
        Assert.assertTrue(userDaoExpected.get("test1").isEmpty());
    }
}