package fr.univlyon1.m1if.m1if13.usersspringboot.model;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.naming.AuthenticationException;

public class UserTest {

    User expectedUser = new User("test", "test");
    private User test;


    @Before
    public void init() {
        this.test = new User("testo", "test");
    }

    @Test
    public void getLogin() {
        Assert.assertEquals("test", expectedUser.getLogin());
    }

    @Test
    public void setLogin() {
        test.setLogin("test");
        Assert.assertEquals("test", expectedUser.getLogin(), test.getLogin());
    }

    @Test
    public void setPassword() {
        test.setPassword("test");
    }

    @Test
    public void isConnected() {
        Assert.assertFalse(expectedUser.isConnected());
    }

    @Test
    public void authenticate() {
        try {
            expectedUser.authenticate("test");
            Assert.assertTrue(expectedUser.isConnected());
        } catch (AuthenticationException e) {
            Assert.fail();
        }
    }

    @Test
    public void disconnect() {
        expectedUser.disconnect();
        Assert.assertFalse(expectedUser.isConnected());
    }
}