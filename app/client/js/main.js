const clientStore = require('../stores/client').default;

const promise = Promise.all( [
    import(/* webpackChunkName: "vue-router" */ 'vue-router'),
    import(/* webpackChunkName: "vuetify" */ 'vuetify'),
    import(/* webpackChunkName: "vue" */ 'vue'),
]);



promise.then(function (plugins) {

    const VueRouter = plugins[0].default;
    const Vuetify = plugins[1].default;
    const Vue = plugins[2].default;


    Vue.use(Vuetify)
    Vue.use(VueRouter);
    Vue.config.productionTip = false;

    const routes = [
        {
            path: '/',
            name: 'home',
            component: () => import(/* webpackChunkName: "vue-files" */ "../components/FormGraph.vue"),
            meta: {
                middleware: [
                    require('../middleware/authenticated.js')
                ]
            }
        },
        {
            path: '/login',
            name: 'login',
            component: () => import(/* webpackChunkName: "vue-files" */"../components/Login.vue")
        },
        {
            path: '/trophies',
            name: 'trophies',
            component: () => import(/* webpackChunkName: "vue-files" */"../components/Trophy.vue"),
            meta: {
                middleware: [
                    require('../middleware/authenticated.js')
                ]
            }
        },
        {
            path: '/win',
            name: 'win',
            component: () => import(/* webpackChunkName: "vue-files" */"../components/Victory.vue"),
            meta: {
                middleware: [
                    require('../middleware/authenticated.js')
                ]
            }
        },
        {
            path: '/lose',
            name: 'lose',
            component: () => import(/* webpackChunkName: "vue-files" */"../components/Defeat.vue"),
            meta: {
                middleware: [
                    require('../middleware/authenticated.js')
                ]
            }
        },
    ];

    const router = new VueRouter({
        mode: "hash",
        routes // short for `routes: routes`
    });

    router.beforeEach(function (to, from, next)  {
        if (to.meta.middleware) {
            const middleware = to.meta.middleware;


            return middleware[0].default([to, next, clientStore, router]);
        }
        return next();
    })

    new Vue({
        router,
        store: clientStore,
        vuetify: new Vuetify,
        el: '#app',
        template: '<App/>',
        components: {
            App: require("../components/App.vue").default,
        },
    });
});



