/* global self */
// fichier : /service-worker.js

var CACHE_VERSION = 1;
var CURRENT_CACHES = {
    prefetch: 'prefetch-cache-v' + CACHE_VERSION
};
var CURRENT_GAME = -1;

// Let us open our database
var database = null;

request = indexedDB.open("leaflet", CACHE_VERSION);

// This handler is called when a new version of the database
// is created, either when one has not been created before
// or when a new version number is submitted by calling
// window.indexedDB.open().
// This handler is only supported in recent browsers.
request.onupgradeneeded = function (eventIDB) {
    database = eventIDB.target.result;

    database.onerror = function (event) {
    };

    // Create an objectStore for this database

    database.createObjectStore("leaflet");

};

const urlsToPrefetch = [
    "/",
    "manifest.json",
    "bundle.js",
    "style-css.js",
    "pure-css.js",
    "main.js",
    "purecss.js",
    "leaflet-css.js",
    "vue-files.js",
    "vendors~vuetify.js",
    "vendors~vuetify-css.js",
    "vendors~main.js",
    "vendors~materialdesignicons.js",
    "vendors~bundle.js",
    "vendors~vue-router.js",
    "img/tomcat_512.png",
    "img/tomcat.png",
    "img/recentrer.ico",
    "runtime.js",
];

console.log("SW: Téléchargement fini.");

self.addEventListener("install", event => {
    console.log("SW: Installation en cours.");


    //console.log('Handling install event. Resources to pre-fetch:', urlsToPrefetch);

    event.waitUntil(
        caches.open(CURRENT_CACHES['prefetch']).then(function (cache) {
            cache.addAll(urlsToPrefetch.map(function (urlToPrefetch) {
                return new Request(urlToPrefetch);
            })).then(function () {
                console.log('All resources have been fetched and cached.');
            });
        }).catch(function (error) {
            console.error('Pre-fetching failed:', error);
        })
    );
    self.skipWaiting();

    // Un Service Worker a fini d'être
    // installé quand la promesse dans
    // `event.waitUntil` est résolue
    event.waitUntil(
        // Création d'une promesse
        // factice qui est résolue au
        // bout d'une seconde
        new Promise((resolve, reject) => {
            setTimeout(() => {
                console.log("SW: Installé.");
                resolve();
            }, 1000);
        })
    );
});


self.addEventListener('fetch', function (event) {

        if (event.request.url.startsWith('chrome-extension')
            || (urlsToPrefetch.indexOf(event.request.url.replace(event.request.referrer, "")) === -1
                && event.request.referrer)
        ) {
            if (event.request.url.startsWith('https://api.tiles.mapbox.com/v4/') ||
                event.request.url.endsWith('.ttf') ||
                event.request.url.endsWith('.woff2') ||
                event.request.url.endsWith('.woff')
            ) {

                event.respondWith(async function () {
                    const element = database.transaction("leaflet").objectStore("leaflet").get(event.request.url)

                    return new Promise((resolve, reject) => {
                        element.onsuccess = function (ev) {

                            if (ev.target.result) {
                                resolve(new Response(ev.target.result, {"status": 200}));
                            } else {
                                fetch(event.request.url).then(function (response) {
                                    return response.blob()
                                }).then(function (blob) {

                                    if (database) {
                                        var transaction = database.transaction(["leaflet"], "readwrite");

                                        if (transaction) {
                                            // affiche le succès de l'ouverture de la transaction
                                            transaction.oncomplete = function (event) {
                                            };

                                            transaction.onerror = function (event) {
                                                console.log(event.value);
                                            };

                                            // On peut maintenant accéder au magasin d'objet
                                            var objectStore = transaction.objectStore("leaflet")

                                            objectStore.put(blob, event.request.url);
                                        }
                                    }
                                    resolve(new Response(blob, {"status": 200}));
                                });
                            }
                        }
                    });
                }());
            }
        } else {


            event.respondWith(caches.open(CURRENT_CACHES['prefetch']).then(function (cache) {
                return cache.match(event.request).then(function (response) {
                    if (response) {
                        //console.log('Service Worker: Returning Cached Response', response);
                        return response;
                    } else {
                        fetch(event.request).then(function (response) {
                            console.log('Service Worker: Returning Response from Server', response);
                            cache.put(event.request, response.clone());
                            return response;
                        }).catch((error) => {
                            //console.log('Fetch failed; returning offline page instead.', error);
                            return caches.match('/');
                        });
                    }
                });
            }));

        }
    }
);


self.addEventListener("activate", event => {
    //console.log("SW: Activation en cours.");

    // Décommentez la ligne suivante
    // pour utiliser le Service Worker
    // au premier chargement de la page
    // self.clients.claim();

    // Un Service Worker a fini d'être
    // activé quand la promesse dans
    // `event.waitUntil` est résolue
    event.waitUntil(
        // Création d'une promesse
        // factice qui est résolue au
        // bout d'une seconde
        new Promise((resolve, reject) => {
            setTimeout(() => {
                console.log("SW: Activé.");
                resolve();
            }, 1000);
        })
    );
});

self.addEventListener("message", event => {

    if (event.data) {
        CURRENT_GAME = JSON.parse(event.data).id;
    } else {
        CURRENT_GAME = -1;
    }

});


self.addEventListener('push', function (event) {

    if (!(Notification && Notification.permission === 'granted')) {
        return;
    }

    var data = {};
    if (event.data) {
        data = event.data.json();
    }


    var title = data.title || "Something Has Happened";
    var message = data.message || "Here's something you might want to check out.";
    var icon = "images/new-notification.png";


    var options = {
        body: message,
        tag: 'simple-push-demo-notification',
        icon: icon
    };

    event.waitUntil(self.registration.showNotification(title, options));
});

self.addEventListener('notificationclick', function (event) {
    let url = 'https://192.168.75.35/';
    event.notification.close(); // Android needs explicit close.
    event.waitUntil(
        clients.matchAll({type: 'window'}).then(windowClients => {
            // Check if there is already a window/tab open with the target URL
            for (var i = 0; i < windowClients.length; i++) {
                var client = windowClients[i];
                // If so, just focus it.
                if (client.url === url && 'focus' in client) {
                    return client.focus();
                }
            }
            // If not, then open the target URL in a new window/tab.
            if (clients.openWindow) {
                return clients.openWindow(url);
            }
        })
    );
});
