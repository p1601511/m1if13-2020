

if (!window.Promise) {
    require('@babel/polyfill/dist/polyfill.min.js');
}

//css
import(/* webpackChunkName: "style-css" */ './css/style.css');
import(/* webpackChunkName: "pure-css" */ 'purecss/build/pure-min.css');

//js
import(/* webpackChunkName: "purecss" */ 'purecss/index.js');
import './js/install_worker.js';


L.Icon.Default.imagePath = '.';

L.Icon.Default.mergeOptions({
    iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
    iconUrl: require('leaflet/dist/images/marker-icon.png'),
    shadowUrl: require('leaflet/dist/images/marker-shadow.png'),
});

import(/* webpackChunkName: "main" */ './js/main.js');

import(/* webpackChunkName: "vuetify-css" */ 'vuetify/dist/vuetify.min.css');
import(/* webpackChunkName: "materialdesignicons" */ '@mdi/font/css/materialdesignicons.min.css');
import(/* webpackChunkName: "leaflet-css" */ 'leaflet/dist/leaflet.css');



