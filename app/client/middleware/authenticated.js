export default function middleware(tab) {

    //tab =[to,  next, clientStore, router ]

    const to = tab[0];
    const next = tab[1];
    const clientStore = tab[2];
    const router = tab[3];

    const origin = window.location.origin;
    const token = sessionStorage.getItem('token');

    var authenticated = false;

    if (navigator.onLine) {
        $.ajax({
            type: "GET",
            url: "https://192.168.75.35/users/authenticate?token=" + token + "&Origin=" + origin,
            // url		: "https://localhost:8081/authenticate?token=" + token + "&Origin=" + origin,
            async: false,
            success: function () {
                clientStore.commit("commit_isAuthenticatate", true);
                clientStore.dispatch("updateData");
                authenticated = true;
            },
            error: function () {
                clientStore.commit("commit_isAuthenticatate", false);

            }
        });
    } else {
        clientStore.commit("commit_isAuthenticatate", false);
    }

    return authenticated ? next() : next({
            name: 'login'
    });
}