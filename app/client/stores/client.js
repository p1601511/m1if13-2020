import Vuex from 'vuex';
import Vue from 'vue';

Vue.use(Vuex);


export default new Vuex.Store({
    state: {
        login: localStorage.getItem("login"),
        isAuthenticate: false,
        pos: {
            longitude: 0,
            latitude: 0,
        },
        marker: [
            {
                longitude: 4.86559,
                latitude: 45.78207,
                text: 'Entrée du bâtiment<br><strong>Nautibus</strong>.'
            }
        ],
        noGameData: null,
        activeGame: false,
        zoom: 0,
        game: [],
    },
    strict: true,
    mutations: {
        commit_pos: function (state, data) {
            const json = JSON.parse(data);
            state.pos.longitude = json.longitude;
            state.pos.latitude = json.latitude;
        },
        commit_zoom: function (state, zoom) {
            state.zoom = zoom;
        },
        commit_isAuthenticatate: function (state, isAuthenticatate) {
            state.isAuthenticate = isAuthenticatate;
        },
        commit_saved_markers: function (state, data) {
            state.savedMarkers = data;
        },
        commit_logout: function (state) {
            state.isAuthenticate = false;
            sessionStorage.removeItem('token');
        },
        update_game: function (state, data) {
            if (data.id === undefined) { //TODO pour l'instant on vérifie comme ça
                state.game = {};
                state.activeGame = false;

                //TODO faire la désabonnement à un flux de partie

                state.noGameData = data;
            } else {
                state.game = data;
                state.noGameData = null;


                // si la partie n'était pas activé on s'enregistre au centre de notification de la partie
                if (state.activeGame === false && navigator.serviceWorker) {
                    if (navigator.serviceWorker.controller) {
                        navigator.serviceWorker.controller.postMessage(JSON.stringify(data));
                    }
                }
                state.activeGame = true;
            }
        },
        commit_login: function (state, login) {
            state.login = login;
            localStorage.setItem("login", login);
        },
        commit_add_win_loose: function (state) {
            if (state.game.id) {
                if (!localStorage.getItem("winloose")) {
                    localStorage.setItem("winloose", JSON.stringify({v: []}));
                }
                var array = JSON.parse(localStorage.getItem("winloose")).v;
                array.push(state.game.id);
                localStorage.setItem("winloose", JSON.stringify({v: array}));
            }
        }
    },
    getters: {
        getTrophies: function (state, getters) {
            let player = getters.getCurrentPlayer;
            return player ? player.trophys : [];
        },
        isAuthenticate: function (state) {
            return state.isAuthenticate
        },
        getMarkers: function (state) {
            return state.game.markers
        },
        getPlayers: function (state) {
            return state.game ? state.game.players : [];
        },
        getCurrentPlayer: function (state) {
            if (state.activeGame) {
                return state.game.players.filter(e => e.id === state.login)[0]
            } else if (state.noGameData) {
                return state.noGameData.players[state.login];
            } else {
                return null;
            }
        },
        getActiveGame: function (state) {
            return state.activeGame
        },
        getLogin: function (state) {
            return state.login
        },
        getWinners: function (state) {
            return state.game.winners ? state.game.winners : [];
        },
        getLosers: function (state) {
            return state.game.losers ? state.game.losers : [];
        },
        getGameStarted: function (state) {
            return state.game.started;
        },
        checkWinLoose: function (state) {
            if (!state.noGameData) {
                if (state.game.id) {
                    if (!localStorage.getItem("winloose")) {
                        localStorage.setItem("winloose", JSON.stringify({v: [] }));
                    }
                    return !JSON.parse(localStorage.getItem("winloose")).v.includes(state.game.id);
                } else {
                    return false;
                }
            } else {
                return false;
            }
        },
        getPolygone: function (state) {
            return state.game.polygon;

        }
    },
    actions: {
        updateData: function (actionContext) {

            const getters = actionContext.getters;
            const commit = actionContext.commit;
            var posEnable = null;
            const pseudo = getters.getLogin;

            const options = {
                enableHighAccuracy: false,
                timeout: 5000,
                maximumAge: 0
            };

            const interval = setInterval(function () {

                if (!getters.isAuthenticate) {
                    clearInterval(interval);
                } else {
                    $.ajax({
                        url: "https://192.168.75.35/api/resources?player=" + pseudo,
                        cors: true,
                        method: "GET",
                        success: function (data) {
                            commit('update_game', data);
                        }
                    });

                    if (getters.isAuthenticate && pseudo && !posEnable) {
                        if (navigator.geolocation) {

                            posEnable = navigator.geolocation.watchPosition(function (position) {

                                if (position.coords.latitude && position.coords.latitude) {
                                    const playerLating = {
                                        lat: parseFloat(position.coords.latitude),
                                        lng: parseFloat(position.coords.longitude)
                                    };

                                    $.ajax({
                                        method: 'put',
                                        url: 'https://192.168.75.35/api/resources/' + pseudo + '/position',
                                        data: playerLating,
                                        headers: {
                                            Authorization: sessionStorage.getItem('token')
                                        }
                                    })
                                }
                            }, function () {
                                navigator.geolocation.clearWatch(posEnable);
                            }, options);
                        }
                    }
                }
            }, 2000);
        },
    },
});