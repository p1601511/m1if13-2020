const path = require('path');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const HtmlWebpackPlugin = require("html-webpack-plugin");
const ScriptExtHtmlWebpackPlugin = require('script-ext-html-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const ServiceWorkerWebpackPlugin = require('serviceworker-webpack-plugin');
const webpack = require("webpack");

module.exports = {
    mode: "production",
    entry: {
        "bundle": "./index.js",
    },
    devtool: 'source-map',
    optimization: {
        runtimeChunk: 'single',
        splitChunks: {
            chunks: 'all',
        },
        minimize: true
    },
    output: {
        path: path.resolve(__dirname, "./dist"),
        filename: '[name].js',
        chunkFilename: '[name].js',
    },
    plugins: [
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            L: 'leaflet'
        }),
        new VueLoaderPlugin(),
        new CopyPlugin([
            { from: 'img', to: 'img' },
            { from: 'manifest.json', to: '' },
        ]),
        new HtmlWebpackPlugin({
            template: 'index.html',
            minify: {
                collapseWhitespace: true,
                // See: https://github.com/jantimon/html-webpack-plugin/issues/1036#issuecomment-421577653
                conservativeCollapse: true,
                removeComments: true,
                removeRedundantAttributes: true,
                removeScriptTypeAttributes: true,
                removeStyleLinkTypeAttributes: true,
                useShortDoctype: true,
            },
        }),
        new ServiceWorkerWebpackPlugin({
            entry: path.join(__dirname, 'service-worker.js'),
        }),
        new ScriptExtHtmlWebpackPlugin({
            defaultAttribute: 'defer'
        }),
    ],
    performance: {
        hints: process.env.NODE_ENV === 'production' ? "warning" : false
    },
    node: {fs: 'empty'},
    resolve: {
        alias: {
            vue: 'vue/dist/vue.js'
        }
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [
                    'style-loader',
                    'css-loader',
                ],
            },
            {
                test: /\.(png|svg|jpg|gif)$/,
                loader: 'file-loader',
                options: {
                    name: '[name].[ext]',
                    esModule: false,
                },
            },
            {
                test: /\.vue$/,
                loader: 'vue-loader'
            },
            {
                test: /\.(woff|woff2|eot|ttf)$/,
                loader: 'url-loader?limit=100000',
            },
        ],
    },
};
