import Vue from 'vue'
import Vuex from 'vuex'
const axios = require('axios').default;

Vue.use(Vuex);

const clientStore = new Vuex.Store({
    state: {
        isAuthenticate: false,
        pos: {
            longitude: 0,
            latitude: 0,
        },
        marker: [
            {
                longitude: 4.86559,
                latitude: 45.78207,
                text: 'Entrée du bâtiment<br><strong>Nautibus</strong>.'
            }
        ],
        zoom: 0,
        games: [],
        players: [],
        dataSpring: [],
        updateData: false,
    },
    strict: true,
    mutations: {
        commit_pos(state, data) {
            const json = JSON.parse(data);
            state.pos.longitude = json.longitude;
            state.pos.latitude = json.latitude;
        },
        commit_trophy(state, data) {
            const json = JSON.parse(data);
            state.trophies = json.trophies;
        },
        commit_zoom(state, zoom) {
            state.zoom = zoom;
        },
        commit_isAuthenticatate(state, isAuthenticatate) {
            state.isAuthenticate = isAuthenticatate;
        },
        commit_saved_markers(state, data) {
            state.savedMarkers = data;
        },
        commit_logout(state) {
          state.isAuthenticate = false;
          sessionStorage.removeItem('token');
        },
        update_games(state, data) {
            state.games = data;
        },
        update_players(state, data) {
            state.players = data;
        },
        update_updateData(state, data) {
            state.updateData = data;
        },
        clear_data_spring(state) {
            state.dataSpring = [];
        },
        commit_add_data_spring(state, dataPlayer){
            state.dataSpring.push(dataPlayer);
        }
    },
    getters: {
        getTrophies: state => state.trophies,
        isAuthenticate: state => state.isAuthenticate,
        getMarkers: state => state.marker,
        getPlayers: state => state.players,
        getElements: state => state.games.map(e => e.markers).reduce((a,b) => a.concat(b)),
        getGames: state => state.games,
        getDataSpring: state => state.dataSpring,
        getPlayer: (state) => (login) => state.players[login],
        getPlayersInGame: (state,getters) => (gameName) => {
            const game = getters.getGame(gameName);
            return game ? game.players.map(e => state.players[e]).filter(
                e => e != null
            ) : [];
        },
        getPlayersOutGame: (state,getters) => (gameName) => {
            const game = getters.getGame(gameName);
            return game && state.players ? Object.values(state.players).filter(p => game.players.indexOf(p.id) === -1) : [];
        },
        getGame: (state) => (gameName) => state.games.filter(e => e.id === gameName)[0],
        updateData: state => state.updateData,
        getPolygone: (state,getters) => (gameName) => {
            return getters.getGame(gameName).polygon;
        }
    },
    actions: {
        updateDataSpring({ commit, getters }) {
            $.ajax({
                url: "https://192.168.75.35/users/users",
                cors: true,
                method: "GET",
                success : function(data) {
                    commit('clear_data_spring');
                    for(var i = 0; i < data.users.length; i++) {
                        $.ajax({
                            url: "https://192.168.75.35/users/user/" + data.users[i],
                            cors: true,
                            method: "GET",
                            success : function(dataPlayer) {
                                commit('commit_add_data_spring', dataPlayer);

                            }
                        });
                    }
                }
            });
        },

        updateData ({ commit, getters  }) {

            if(! getters.updateData) {
                commit("update_updateData", true);
                const interval = setInterval(() => {
                    if (!getters.isAuthenticate) {
                        clearInterval(interval);
                        commit("update_updateData", false);
                    } else {
                        $.ajax({
                            url: "https://192.168.75.35/api/resources",
                            cors: true,
                            method: "GET",
                            success: function (data) {
                                commit('update_players', data.players);
                                commit('update_games', data.games);
                            }
                        });
                    }
                }, 2000);
            }
        }
    }
});

export default clientStore;

