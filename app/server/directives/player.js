import Vue from 'vue';

// Register a global custom directive called `v-focus`
export default Vue.directive('player', {
    // When the bound element is inserted into the DOM...s

    bind: function(el) {
        console.log(el);
    },

    inserted: function (el) {
        // Focus the element

        el.focus()
    }
});