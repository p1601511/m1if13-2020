
module.exports = class Marker {


    constructor(id, position, round, icon, game) {
        this.id = id;
        this.position = position;
        this.round = round;
        this.icon = icon;
        this.game = game;
    }

    toArray() {
        return {
            id: this.id,
            icon: this.icon,
            round: this.round,
            game: this.game,
            position: this.position.toArray(),
        }
    }

    setPosition(lat,long) {
        this.position.setLat(lat);
        this.position.setLong(long);
    }

    setIcon(icon) {
        this.icon = icon;
    }

    getPosition() {
        return this.position;
    }

    getIcon() {
        return this.icon;
    }

    getId() {
        return this.id
    }

    getRound() {
        return this.round;
    }

    getGame() {
        return this.game;
    }
}