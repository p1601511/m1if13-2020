const Player = require('./Player.js');
const LatLng = require('./LatLng.js');
const Game = require('./Game.js');


module.exports = class Database {

    constructor() {
        this.dataGames = {};
        this.dataPlayers = {};
        this.trophies = [];
        this.idMarker = 0;
    }

    /**
     * Add user.
     * @param login
     */
    addUserData(login) {
        this.dataPlayers[login] = this.getDefaultData(login);
    }

    checkUserExist(login) {
        return this.dataPlayers[login] !== undefined;
    }

    checkGameExist(name) {
        return this.dataGames[name] !== undefined;
    }

    getGame(name) {
        return this.dataGames[name];
    }


    affectGame(login,gameId, webPush) {

        const listGame = Object.values(this.dataGames).filter(e => e.checkUserExist(login));

        if (listGame.length !== 0) {

            for (let i = 0; i < listGame.length; i++) {
                this.leaveGame(login, listGame[i]);
            }
        }

        this.dataGames[gameId].addPlayer(login);

        if (webPush) {
            this.dataPlayers[login].sendNotification(webPush, {
                title: 'Préparez vous !',
                message: "Vous avez rejoins une nouvelle partie !"
            })
        }
    }

    leaveGame(login,game) {
        game.removePlayer(login);
    }

    createIfNotExist(login) {
        if (!this.checkUserExist(login)) {
            this.addUserData(login);
        }
    }

    insertPlayer(data) {
        this.createIfNotExist(data.id);
        this.updatePlayer(data);
    }

    updatePlayer(data) {
        this.dataPlayers[data.id].setAvatar(data.avatar);
        this.dataPlayers[data.id].setTTL(data.ttl);
        this.dataPlayers[data.id].setStatus(data.status);
    }

    removePlayer(login) {
        delete this.dataPlayers[login];
        const game = Object.values(this.dataGames).filter(g => g.checkUserExist(login));
        if (game) {
            game[0].removePlayer(login);
        }
    }

    setSubscriptionPlayer(login, subscription) {
        this.dataPlayers[login].setSubscription(subscription);
    }

    sendNotificationPlayer(login, payload, webPush) {
        this.dataPlayers[login].sendNotification(webPush, payload);

    }

    updatePosition(login, data, webPush) {
        this.dataPlayers[login].setPosition(data.lat, data.lng);
        var playerPos = new LatLng(data.lat, data.lng);
        const game = this.getGameByPseudo(login);
        if (game != null) {
            /*
            if(!this.dataGames[game.id].isMarkerInsidePolygon(playerPos)) {
                this.dataPlayers[login].setStatus("dead");
                this.dataGames[game.id].addLoser(this.dataPlayers[login]);
            }
             */
            this.dataGames[game.id].checkElementTook(this.dataPlayers[login], Object.values(this.dataPlayers), webPush);
        }
    }

    updateImage(login, data) {
        this.dataPlayers[login].setAvatar(data[0], data[1]);
    }


    add_Trophy(login, data) {
        this.dataPlayers[login].addTrophy(data);
    }

    addPointTrophy(login, data) {
        this.dataPlayers[login].trophys[data].addPoints();
    }

    getPlayerTrophies(login) {
        return this.dataPlayers[login].trophys;
    }

    getPlayers() {
        return this.dataPlayers;
    }

    getAllData() {

        return {
            players: this.dataPlayers,
            games: Object.values(this.dataGames).map(function (element) {
                return element.toArray();
            })
        }
    }

    getGameByPseudo(pseudo) {
        const game = Object.values(this.dataGames).filter(g => g.checkUserExist(pseudo));
        if (game.length) {
            return game[0]
        } else {
            return null;
        }
    }

    getDataWithFilter(pseudo) {
        const game = this.getGameByPseudo(pseudo);

        if (game) {
            return game.toArrayWithPlayers(this.dataPlayers);
        }
        else {
            return {
                players: this.dataPlayers,
            };
        }
    }

    testGetDIstance() {
        var myMarker = new LatLng(45, 35);
        var dest = new LatLng(10,10);

        var dist = myMarker.getDistance(dest);

        console.log("la distance test est de : " + dist);
    }

    getDistance2Points(position1,position2) {
        var from = new LatLng(position1[0], position1[1]);
        var to = new LatLng(position2[0], position2[1]);

        return from.getDistance(to);
    }

    deleteElement() {
        for (var element in this.dataGames.elements) {
            //récupérer les positions
        }
    }

    addGame(data) {
        this.dataGames[data.id] = new Game(data.id);
    }

    updateGame(name, data) {
        this.dataGames[name].update(data)
    }

    addMarkerInGame(name, data) {

        if (!this.dataGames[name].isStarted()) {
            this.dataGames[name].addMarker(data.position, data.icon, data.round, this.idMarker);
            this.idMarker++;
            return true;
        } else {
            return false;
        }
    }

    removeMarkerInGame(name, id) {
        if (!this.dataGames[name].isStarted()) {
            this.dataGames[name].removeMarker(parseInt(id))
            return true;
        } else {
            return false;
        }
    }

    startGame(name) {
        this.dataGames[name].start()
    }

    stopGame(name) {
        this.dataGames[name].stop()
    }

    removeGame(name) {
        delete this.dataGames[name];
    }

    getDefaultData(login) {
        return new Player (
            login,
            null,
            0,
            "alive",
            new LatLng(0,0)
        )
    }
}