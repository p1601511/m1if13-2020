

module.exports = class Trophy {


    constructor(id) {
        this.id = id;
        this.description = "";
        this.points = 0;
        this.medaille = "";
    }

    setPoints(pts) {
        this.points = pts;
    }

    setDescription(des) {
        this.description = des;
    }

    setId(id) {
        this.id = id;
    }

    setMedaille(url) {
        this.medaille = url;
    }

    addPoints(){
        this.points++;
    }
}