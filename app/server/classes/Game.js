const Marker = require('./Marker.js');
const LatLng = require('./LatLng.js');
const inside = require('point-in-polygon');
module.exports = class Game {


    constructor(id) {
        this.id = id;
        this.players = [];
        this.elements = {};
        this.round = 1;
        this.started = false;
        this.polygone = [];
        this.winners = [];
        this.losers = [];
        this.notifWinLoose = false;
    }

    getId() {
        return this.id;
    }

    nextRound() {
        this.round = parseInt(this.round) + 1;
    }

    getRound() {
        return this.round;
    }

    start() {
        this.started = true
    }

    setPolygon(polygon) {
        this.polygone = polygon;
    }

    stop() {
        this.started = false
    }

    resetPlayers(listPlayers) {
        listPlayers.filter(e => this.players.includes(e.getId()) )
            .forEach(function (player) {
                player.setStatus("alive");
            })
    }

    addPlayer(login) {
        this.players.push(login);
    }

    removePlayer(login) {
        this.players.splice(this.players.indexOf(login), 1);
    }

    addMarker(position, icon, round, idMarker) {
        const pos = new LatLng(position[0], position[1]);
        if (Object.values(this.elements).filter(e => e.getPosition().equals(pos)).length === 0) {
            this.elements[idMarker] = new Marker(idMarker, pos, round, icon, this.getId());
        }
    }

    isStarted() {
        return this.started;
    }

    removeMarker(id) {
        delete this.elements[id];
    }

    checkUserExist(login) {
        return this.players.indexOf(login) !== -1;
    }

    endGame() {
        if (!this.started) {

        }
    }

    checkElementTook(player, listPlayers, webPush) {

        if (this.isStarted()) {
            var posJoueur = player.getPosition();

            if (player.getStatus() === "alive") {
                for (var i in this.elements) {
                    var element = this.elements[i];
                    if (this.getRound() === element.getRound()) {
                        if (element.getPosition().getDistance(posJoueur) < 2) {
                            player.setElement(element);
                            this.removeMarker(element.id);
                            const payloadWin = JSON.stringify({
                                title: 'Victoire !',
                                message: "Vous avez gagné !"
                            });
                            player.sendNotification(webPush, {
                                title: 'Un marker de gagné',
                                message: "Vous avez gagné un marker !"
                            })
                        }
                    }
                }
            }

            if (Object.values(this.elements).length === 0) {
                for (var i in listPlayers) {
                    if (this.players.includes(listPlayers[i].getId())) {
                        this.winners.push(listPlayers[i]);
                    }
                }
                this.stop();
                this.resetPlayers(listPlayers)
            } else if (Object.values(this.elements).filter(e => e.getRound() === this.getRound()).length === 0) {
                for (var i in listPlayers && this.players.includes(player.getId())) {
                    var playerElement = listPlayers[i].getElements().filter(e =>
                        e.getRound() === this.getRound() && e.getGame() === this.getId());
                    if (playerElement.length === 0) {
                        this.losers.push(listPlayers[i]);
                        listPlayers[i].setStatus("dead");
                    }
                }
                if (this.alive(listPlayers) < 2) {
                    this.stop()
                    this.resetPlayers(listPlayers)
                    var listAlive = listPlayers.filter(e => e.getStatus() === "alive" && this.players.includes(e.getId()));
                    for (var i in listAlive) {
                        this.winners.push(listAlive[i]);
                    }
                } else {
                    this.nextRound();
                    for (var i = 0; i < listPlayers; i++) {
                        listPlayers[i].sendNotification(webPush, {
                            title: 'Round suivant !',
                            message: "Attention la difficulté augmente d'un cran"
                        });
                    }
                }

                if (!this.notifWinLoose && !this.isStarted() && webPush) {
                    this.notifWinLoose = true;
                    const payloadWin = JSON.stringify({
                        title: 'Victoire !',
                        message: "Vous avez gagné !"
                    });
                    const payloadLoose = JSON.stringify({
                        title: 'Victoire !',
                        message: "Vous avez gagné !"
                    });
                    for (var i = 0; i < this.winners; i++) {
                        this.winners[i].sendNotification(webPush, payloadWin);
                    }
                    for (var i = 0; i < this.losers; i++) {
                        this.losers[i].sendNotification(webPush, payloadLoose);
                    }
                }
            }
        }
    }

    alive(listPlayers) {
        return listPlayers.filter(e => this.players.indexOf(e.getId()) !== -1 &&
            this.getId && e.getStatus() === "alive").length
    }

    addLoser(player) {
        this.losers.push(player)
    }

    update(data) {
        if (data.round != null) {
            this.round = data.round;
        }
    }

    toArray() {
        return {
            id: this.id,
            players: this.players,
            round: this.round,
            losers: this.losers.map(e => e.getId()),
            winners: this.winners.map(e => e.getId()),
            started: this.started,
            polygon: this.polygone,
            markers: Object.values(this.elements).map(e => e.toArray()),
        }
    }

    toArrayWithPlayers(dataPlayers) {
        return {
            id: this.id,
            players: this.players.map(pseudo => dataPlayers[pseudo]),
            round: this.round,
            losers: this.losers.map(e => e.getId()),
            winners: this.winners.map(e => e.getId()),
            started: this.started,
            polygon: this.polygone,
            markers: Object.values(this.elements)
                .filter(e => e.getRound() === this.getRound())
                .map(e => e.toArray()),
        }
    }

    sendNotification(webPush, payload, dataPlayers) {
        for (let i = 0; i < this.players.length; i++) {
            dataPlayers[this.players[i]].sendNotification(webPush, payload);
        }
    }

    isMarkerInsidePolygon(marker) {

        if (this.isStarted() && this.polygone) {

            var polyPoints = this.polygone[0].map(e => [e.lat, e.lng])
            var point = [ parseFloat(marker.latitude), parseFloat(marker.longitude)]

            return inside(point, polyPoints);
        } else {
            return true;
        }
    };
}