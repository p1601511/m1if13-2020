const Trophy = require('./Trophy');
module.exports = class Player {



    constructor(id, avatar, ttl, status, position) {

        //trophies init
        var firstB = new Trophy("Fist Blood");
        var flash = new Trophy("The Flash");
        var lastM = new Trophy("Last Minute");

        firstB.setDescription("Vous avez fait le premier sang dans une partie.");
        flash.setDescription("Vous avez été le plus rapide dans une partie.");
        lastM.setDescription("Vous arrivez juste avant la fin du temps imparti.");

        this.id = id;
        this.avatar = avatar;
        this.ttl = ttl;
        this.status = status;
        this.position = position;
        this.elements = [];
        this.trophys = [];
        this.subscription = null;
        this.trophys.push(firstB, flash, lastM);

    }

    toArray() {
        return {
            id: this.id,
            avatar: this.avatar,
            ttl: this.ttl,
            status: this.status,
            position: this.position.toArray(),
            trophys: this.trophys.toArray()
        }
    }

    getId() {
        return this.id;
    }

    setElement(element) {
        this.elements.push(element);
    }

    getElements() {
        return this.elements;
    }

    getPosition() {
        return this.position;
    }

    setPosition(lat,long) {
        this.position.setLat(lat);
        this.position.setLong(long);
    }

    setAvatar(avatar) {
        this.avatar = avatar;
    }

    setTTL(ttl) {
        this.ttl = ttl;
    }

    setSubscription(subscription) {
        this.subscription = subscription;
    }

    sendNotification(webPush, payload) {
        if(this.subscription) {
            webPush.sendNotification(this.subscription, payload, {
                proxy: "http://proxy.univ-lyon1.fr:3128"  // my proxy server
            }).catch(error => {
                console.log(error);
                console.error(error.stack);
            });
        }
    }

    setStatus(status) {
        this.status = status;
    }

    getStatus() {
        return this.status;
    }

    addTrophy(trophy){
        this.trophys.push(trophy);
    }

}