

module.exports = class LatLng {


    constructor(latitude, longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    setLat(lat) {
        this.latitude = lat;
    }

    setLong(long) {
        this.longitude = long;
    }

    equals(position) {
        return this.latitude === position.latitude && this.longitude === position.longitude;
    }


    toRadian(degree) {
        return degree*Math.PI/180;
    }

    getDistance(destination) {
        // return distance in meters
        var lon1 = this.toRadian(this.longitude),
            lat1 = this.toRadian(this.latitude),
            lon2 = this.toRadian(destination.longitude),
            lat2 = this.toRadian(destination.latitude);

        var deltaLat = lat2 - lat1;
        var deltaLon = lon2 - lon1;

        var a = Math.pow(Math.sin(deltaLat/2), 2) + Math.cos(lat1) * Math.cos(lat2) * Math.pow(Math.sin(deltaLon/2), 2);
        var c = 2 * Math.asin(Math.sqrt(a));
        var EARTH_RADIUS = 6371;
        return c * EARTH_RADIUS * 1000;
    }

    toArray() {
        return [
            this.latitude,
            this.longitude
        ];
    }
}