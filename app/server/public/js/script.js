
const Controleur = function () {

    const self = this;

    this.getTokenE = function () {
        return sessionStorage.getItem("token");
    }

    this.mustacheGetView = function(id,data) {
        var template = $(id).html();
        Mustache.parse(template);   // optional, speeds up future uses
        return Mustache.to_html(Mustache.render(template, data));
    }

    this.getResources = function (login,  callbackSuccess) {
        fetch("http://192.168.75.35:3376/resources")
            .then((response) => response.json())
            .then((data) => {
                callbackSuccess(data.filter(e => e.id = login)[0]);
            });
    }


    this.renderUser = function(login) {
        fetch("http://192.168.75.35:8080/user/" + login)
            .then((response) => response.json())
            .then((data) => {
                const view = $(self.mustacheGetView("#template-user", data));
                $('#listUser').append(view);

                $(view).find("button[title=Delete]").click(function () {
                    axios.delete("http://192.168.75.35:8080/user/" + data.login)
                        .then(function (response) {
                            self.getViewUser();
                        })
                        .catch(function (fail) {
                            console.log(fail);
                        });
                });

                $(view).find("button[title=Edit]").click(function () {
                    self.getResources(login,function (userData) {
                        const viewEdit = $(self.mustacheGetView("#template-edit", userData));
                        $(viewEdit).find("input[name=form-blurred]").prop("checked", userData.blurred ? "yes" : "no");
                        $(viewEdit).find("select[name=form-role]").val(userData.role);
                        $(viewEdit).find("select[name=form-status]").val(userData.status);

                        $('#form-edit-template').html(viewEdit)

                        location.hash = "edit";
                    });
                });
            });
    }

    this.getDataForm = function(form) {
        return {
            login: $(form).find("input[name=form-login]").val(),
            avatar: $(form).find("input[name=form-avatar]").val(),
            blurred: $(form).find("input[name=form-blurred]").val(),
            role: $(form).find("select[name=form-role]").val(),
            ttl: $(form).find("input[name=form-ttl]").val(),
            status: $(form).find("select[name=form-status]").val(),
        };
    }

    this.submitFormLogin = function(form) {

        const data = {
            login: $(form).find("input[name=form-login]").val(),
            password: $(form).find("input[name=form-password]").val(),
        }

        axios.post("http://192.168.75.35:8080//adminToken?login=" + data.login + "&password=" + data.password )
            .then(function (response) {
                self.getViewUser();
                sessionStorage.setItem("token", response.headers["authorization"]);
                location.hash = "users";
            })
            .catch(function (fail) {
                location.hash = "login";
            });

        console.log(data);

        return false;
    }

    this.submitFormEditUser = function (form) {
        const data = this.getDataForm(form);

        console.log(data);

        return false;
    }

    this.submitFormAddUser = function(form) {

        const data = this.getDataForm(form);

        axios.post("http://192.168.75.35:8080/users", data)
            .then(function (response) {
                self.getViewUser();
                location.hash = "users";
            })
            .catch(function (fail) {
                console.log(fail);
            });

        return false;
    }

    this.getViewUser = function() {
        axios.get("http://192.168.75.35:8080/users")
            .then(function (response) {
                $('#listUser').html("");
                const listUser = response.data.users;
                for (let i = 0; i < listUser.length; i++) {
                    self.renderUser(listUser[i]);
                }
            })
            .catch(function (fail) {
                console.log(fail);
            });
    }

    axios.get("http://192.168.75.35:8080//validAdminToken?token=" + self.getToken() + "&origin=" + location.origin)
        .then(function (response) {
            self.getViewUser();
            location.hash = "users";
        })
        .catch(function (fail) {
            location.hash = "login";
        });


    location.hash = "login";
}

var controleur = new Controleur();



$("#addUser").click(function () {
    location.hash = "add";
})