import Vue from 'vue';
import VueRouter from "vue-router";
import Vuex from 'vuex'
import clientStore from "../stores/client";
import App from "../components/App.vue";


Vue.use(Vuex);
Vue.use(VueRouter);
Vue.config.productionTip = false;

const routes = [
    {
        path: '/',
        name: 'home',
        redirect: '/players'
    },
    {
        path: '/login',
        name: 'login',
        component: require("../components/Login.vue").default
    },
    {
        path: '/player/:login/edit',
        name: 'edit',
        component: require("../components/players/Edit.vue").default,
        meta: {
            middleware: [
                require('../middleware/authenticated.js')
            ]
        }
    },
    {
        path: '/players/add',
        name: 'add',
        component: require("../components/players/Add.vue").default,
        meta: {
            middleware: [
                require('../middleware/authenticated.js')
            ]
        }
    },
    {
        path: '/games/add',
        name: 'add',
        component: require("../components/games/Add.vue").default,
        meta: {
            middleware: [
                require('../middleware/authenticated.js')
            ]
        }
    },
    {
        path: '/game/:id/edit',
        name: 'edit',
        component: require("../components/games/Edit.vue").default,
        meta: {
            middleware: [
                require('../middleware/authenticated.js')
            ]
        }
    },
    {
        path: '/game/:id/view',
        name: 'edit',
        component: require("../components/games/View.vue").default,
        meta: {
            middleware: [
                require('../middleware/authenticated.js')
            ]
        }
    },
    {
        path: '/players',
        name: 'players',
        component: require("../components/players/Players.vue").default,
        meta: {
            middleware: [
                require('../middleware/authenticated.js')
            ]
        }
    },
    {
        path: '/map',
        name: 'map',
        component: require("../components/Map.vue").default,
        meta: {
            middleware: [
                require('../middleware/authenticated.js')
            ]
        }
    },
    {
        path: '/parameter',
        name: 'parameter',
        component: require("../components/Parameter.vue").default,
        meta: {
            middleware: [
                require('../middleware/authenticated.js')
            ]
        }
    },
    {
        path: '/games',
        name: 'games',
        component: require("../components/games/Games.vue").default,
        meta: {
            middleware: [
                require('../middleware/authenticated.js')
            ]
        }
    },

];

const router = new VueRouter({
    mode: "hash",
    routes // short for `routes: routes`
});

router.beforeEach((to, from, next) => {

    if (!to.meta.middleware) {
        return next()
    }
    const middleware = to.meta.middleware;

    const context = {
        to,
        from,
        next,
        clientStore
    };

    return middleware[0].default({
        ...context,
    })
})

new Vue({
    router,
    store: clientStore,
    el: '#app',
    template: '<App/>',
    components: {
        App
    },
    render: h => h(App)
});