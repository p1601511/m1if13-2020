
// img
import '../img/copper.png';
import '../img/diamond.png'
import '../img/gold.png';
import '../img/platinium.png';
import '../img/silver.png';
import '../public/index.html';


function component() {
    const element = document.createElement('div');

    // Lodash, currently included via a script, is required for this line to work
    element.innerHTML = _.join(['Hello', 'webpack'], ' ');

    return element;
}

document.body.appendChild(component());