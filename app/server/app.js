const bodyParser = require('body-parser');
const express = require('express');
const axios = require('axios');
const app = express();
const Database = require('./classes/Database.js');
const webPush = require('web-push');

// VAPID keys should only be generated only once.
//const vapidKeys = webPush.generateVAPIDKeys();
const vapidKeys = {
    publicKey: 'BA6d7V1CqYpXi2R-3WEa6tXNJGzNgU_2Gu84xgZ5C7iQePX2kCx-bY8NcOBjmbDkHSJfHKB1ywdHayd5imzoeEc ',
    privateKey: 'bhsCsncLXD9ydlME3liI7r4a9nhGI8NALGZ5Uh4bkfs',
};

webPush.setVapidDetails(
    'mailto:notif@m1if13-2020.fr',
    vapidKeys.publicKey,
    vapidKeys.privateKey
);



/*
 Base de données
 */
const database = new Database();

var whitelist = [
    'http://localhost:3376',
    'http://localhost:8080',
    'https://192.168.75.35',
    'http://192.168.75.35:3376',
    "https://editor.swagger.io" //TODO pour tester avec l'éditeur de swagger
];

var checkAllowOrigin = function (req) {
    const origin = req.header('Origin');
    if (origin != null) {
        return whitelist.indexOf(origin) !== -1 ||
            whitelist.indexOf(origin.substr(0, origin.length - 1)) !== -1;
    } else {
        return false;
    }
};

// Add headers
app.use(function (req, res, next) {

    if (checkAllowOrigin(req)) {

        // Website you wish to allow to connect
        res.setHeader('Access-Control-Allow-Origin', req.header('Origin'));

        // Request methods you wish to allow
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

        // Request headers you wish to allow
        res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,Authorization');

        // Set to true if you need the website to include cookies in the requests sent
        // to the API (e.g. in case you use sessions)
        res.setHeader('Access-Control-Allow-Credentials', true);

        if (req.method === 'OPTIONS') {
            res.status(200).end();
        } else {
            next();
        }
    } else if (req.header("Origin") == null) {
        next();
    } else {

        res.status(403).end()

    }

});


function checkAuthentication(token, origin, success, fail) {

    axios.get("http://192.168.75.35:8080/authenticate?token=" + token + "&Origin=" + origin)
        .then(success)
        .catch(fail);
}

function checkAdminAuthentication(token, origin, success, fail) {

    axios.get("http://192.168.75.35:8080/validAdminToken?token=" + token + "&origin=" + origin)
        .then(success)
        .catch(fail);
}

function checkLogin(token, login, success, fail) {
    axios.get("http://192.168.75.35:8080/validUserToken?token=" + token + "&login=" + login)
        .then(function () {
            if (database.checkUserExist(login)) {
                success();
            } else {
                fail();
            }
        })
        .catch(fail);
}

function is_url(str)
{
    regexp =  /^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/;
    if (regexp.test(str))
    {
        return true;
    }
    else
    {
        return false;
    }
}

function checkUserRight(req, res, successCallback) {

    if (req.params.resource_id && req.body) {

        checkAuthentication(req.header('Authorization'), req.header('Origin'), function () {

            checkLogin(req.header('Authorization'), req.params.resource_id, successCallback, function () {
                res.status(404).json(
                    {
                        message: "User not found"
                    }
                )
            })

        }, function () {
            res.status(401).json(
                {
                    message: "User authentication failed"
                }
            )
        })
    } else {
        res.status(400).json(
            {
                message: "Erreur, remplissez tous les champs !"
            }
        )
    }
}

function checkAdminRight(req, res, successCallback) {
    if (req.body) {

        checkAdminAuthentication(req.header('Authorization'), req.header('Origin'), successCallback, function () {
            res.status(401).json(
                {
                    message: "Admin authentication failed"
                }
            )
        })
    } else {
        res.status(400).json(
            {
                message: "Erreur, remplissez tous les champs !"
            }
        )
    }
}

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.get('/', function (req, res) {
    res.json('Hello World!')
});

// ROUTES
app.route('/resources')
    .get(function (req, res) {
        if (req.query.player !== undefined && database.checkUserExist(req.query.player)) {
            res.json(database.getDataWithFilter(req.query.player));
        } else {
            res.json(database.getAllData());
        }
    });

app.route('/resources/:resource_id/trophies').get(function (req, res) {
        res.status(200).json({
            trophies : database.getPlayerTrophies(req.params.resource_id)
        });
});

app.route('/resources/:resource_id/position').put(function (req, res) {
    
        checkUserRight(req, res, function () {

            database.updatePosition(req.params.resource_id, req.body, webPush);
            res.json(
                {
                    message: "mise à jour de la position de " + req.params.resource_id,
                    id: req.params.resource_id,
                    position: req.body.position
                }
            )
        })
    }
);

app.route('/resources/:resource_id/image').put(function (req, res) {

    checkUserRight(req, res, function () {
        if(is_url(req.body.avatar)) {
            database.updateImage(req.params.resource_id, req.body);
            res.json(
                {
                    message: "mise à jour de l'URL image de " + req.params.resource_id,
                    id: req.body.resource_id
                }
            );
        } else {
            res.status(401).json(
                {
                    message: "Url non valide."
                }
            );
        }
    });
});

app.route('/resources').post(function (req, res) {

    checkAdminRight(req, res, function () {

        if (!database.checkUserExist(req.body.id)) {
            database.insertPlayer(req.body);
            res.status(201).end();
        } else {
            res.status(401).json(
                {
                    message: "Fail insert"
                }
            )
        }

    });
});

app.route('/resources/:resource_id').put(function (req, res) {

    checkAdminRight(req, res, function () {

        database.updatePlayer(req.body);

        res.status(204).end();
    });
});

app.route('/resources/:resource_id').delete(function (req, res) {

    checkAdminRight(req, res, function () {

        if (database.checkUserExist(req.params.resource_id)) {
            database.removePlayer(req.params.resource_id);
            res.status(204).end();
        } else {
            res.status(404).json(
                {
                    message: "User not found"
                }
            )
        }
    });
});

app.route('/games').post(function (req, res) {

    checkAdminRight(req, res, function () {

        database.addGame(req.body);

        res.status(201).end();
    });
});

app.route('/games/:resource_id').put(function (req, res) {

    checkAdminRight(req, res, function () {

        database.updateGame(req.params.resource_id, req.body);

        res.status(204).end();
    });
});

app.route('/game/:resource_id/markers').put(function (req, res) {

    checkAdminRight(req, res, function () {

        if (req.body.position && req.body.icon) {
            let game = database.addMarkerInGame(req.params.resource_id, req.body);
            if (game) {
                res.status(204).end();
            } else {
                res.status(401).json(
                    {
                        message: "This game has not started"
                    }
                )
            }
        } else {
            res.status(400).json(
                {
                    message: "Bad request"
                }
            )
        }
    });
});

app.route('/game/:resource_id/polygon').put(function (req, res) {

    checkAdminRight(req, res, function () {

        if (req.body) {

            let game = database.getGame(req.params.resource_id);
            if (game) {
                game.setPolygon(req.body)
                res.status(204).end();
            } else {
                res.status(401).json(
                    {
                        message: "Polygon is update"
                    }
                )
            }
        } else {
            res.status(400).json(
                {
                    message: "Bad request"
                }
            )
        }
    });
});

app.route('/game/:resource_id/marker/:marker_id').delete(function (req, res) {

    checkAdminRight(req, res, function () {

        let game = database.removeMarkerInGame(req.params.resource_id, req.params.marker_id);
        if (game) {
            res.status(204).end();
        } else {
            res.status(401).json(
                {
                    message: "This game has not started"
                }
            )
        }
    });
});


app.route('/game/:resource_id/start').put(function (req, res) {

    checkAdminRight(req, res, function () {

        database.startGame(req.params.resource_id);

        const payload = JSON.stringify({
            title: 'Une partie a commencé',
            message: "Une partie a commencé tenez vous prêt"
        });

        const game = database.getGame(req.params.resource_id);

        game.sendNotification(webPush, payload, database.getPlayers() );

        res.status(204).end();
    });
})

app.route('/game/:resource_id/stop').put(function (req, res) {

    checkAdminRight(req, res, function () {

        database.stopGame(req.params.resource_id);

        res.status(204).end();
    });
});

app.route('/games/:resource_id/join').put(function (req, res) {

    checkAdminRight(req, res, function () {

        if (database.checkUserExist(req.body.id)) {
            if (database.checkGameExist(req.params.resource_id)) {
                database.affectGame(req.body.id, req.params.resource_id, webPush);
                res.status(204).end();
            } else {
                res.status(404).json(
                    {
                        message: "Game not found"
                    }
                )
            }
            res.status(204).end();
        } else {
            res.status(404).json(
                {
                    message: "User not found"
                }
            )
        }

    });
});

app.route('/games/:resource_id/leave/:player_id').delete(function (req, res) {

    checkAdminRight(req, res, function () {

        if (database.checkUserExist(req.params.player_id)) {
            if (database.checkGameExist(req.params.resource_id)) {
                database.leaveGame(req.params.player_id, database.getGame(req.params.resource_id));
                res.status(204).end();
            } else {
                res.status(404).json(
                    {
                        message: "Game not found"
                    }
                )
            }
            res.status(204).end();
        } else {
            res.status(404).json(
                {
                    message: "User not found"
                }
            )
        }

    });
});

app.route('/games/:resource_id').delete(function (req, res) {

    checkAdminRight(req, res, function () {

        if (database.checkGameExist(req.params.resource_id)) {

            database.removeGame(req.params.resource_id);

            res.status(204).end();

        } else {
            res.status(404).json(
                {
                    message: "Game not found"
                }
            )
        }
    });
});


app.route('/resources/:resource_id/subscription').post( function (req, res) {

    checkUserRight(req, res, function () {
        const subscription = req.body;

        database.setSubscriptionPlayer(req.params.resource_id, subscription)

        res.status(201).json({});
        const payload = JSON.stringify({
            title: 'Bienvenue sur M1IF13',
            message: "Le flux de notification est activé"
        });

        database.sendNotificationPlayer(req.params.resource_id, payload, webPush);
    });
});


app.use(express.static('dist'));

app.use(function (req, res, next) {

    res.status(404).send("We can't find you page you requested, sorry.")
});

app.listen(3376, function () {
    console.log('Example app listening on port 3376!')
});

// database.insertPlayer({
//     id: "test",
//     status: "alive"
// })
// database.insertPlayer({
//     id:"flo",
//     status: "alive"
// })
// database.addGame({
//     id: "testgame"
// })
// database.affectGame("test", "testgame")
// database.affectGame("flo", "testgame")
//
// database.getGame("testgame").setPolygon(
//     [
//         [
//             {
//                 "lat": 35.690380546329266,
//                 "lng": 139.6906685829163
//             },
//             {
//                 "lat": 35.6906419587616,
//                 "lng": 139.6936082839966
//             },
//             {
//                 "lat": 35.68865520278908,
//                 "lng": 139.6939301490784
//             },
//             {
//                 "lat": 35.68853320738875,
//                 "lng": 139.69107627868655
//             }
//         ]
//     ]);
// database.addMarkerInGame("testgame", {
//     round: 1,
//     position: [35.68894276263738,139.6930128335953 ]
// })
// database.addMarkerInGame("testgame", {
//     round: 2,
//     position: [0,1 ]
// })
// database.startGame("testgame")
// database.updatePosition("test", {lat:35.68894276263738, lng: 139.6930128335953})
// console.log(database.getGame("testgame"))