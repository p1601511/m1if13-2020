import clientStore from "../stores/client";

const axios = require('axios').default;

export const authenticated = function () {
    // Si l'utilisateur n'est pas authentifié

    const origin = window.location.origin;
    const token = sessionStorage.getItem('token');
    var isAuthenticated = false;


    $.ajax ({
        type	: "GET",
        url		: "https://192.168.75.35/users/validAdminToken?token=" + token + "&origin=" + origin,
        async	: false,
        success	: () => { isAuthenticated = true },
        error: () => { isAuthenticated = false }
    });

    return isAuthenticated;

};

export default function middleware({ next, clientStore }) {

    const isAuthenticated = authenticated();

    clientStore.commit( "commit_isAuthenticatate", isAuthenticated);
    clientStore.dispatch("updateData");
    clientStore.dispatch("updateDataSpring");
    return isAuthenticated ? next() : next({
        path: "login",
    });
}