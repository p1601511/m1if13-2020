const path = require('path');
const HtmlWebpackPlugin = require("html-webpack-plugin");
require("webpack-dev-server");
const CopyPlugin = require('copy-webpack-plugin');
const webpack = require("webpack");

module.exports = {
    mode: "production",
    entry: {
        "bundle": "./index.js",
    },
    devtool: 'source-map',
    optimization: {
        runtimeChunk: 'single',
        splitChunks: {
            chunks: 'all',
        },
        minimize: true
    },
    output: {
        path: path.resolve(__dirname, "./dist"),
        filename: '[name].js',
        chunkFilename: '[name].js',
    },
    plugins: [
        new CopyPlugin({
            patterns: [
                {from: 'out', to: 'out'},
            ],
        }),
        new HtmlWebpackPlugin({
            template: './src/main.html',
            minify: {
                collapseWhitespace: true,
                // See: https://github.com/jantimon/html-webpack-plugin/issues/1036#issuecomment-421577653
                conservativeCollapse: true,
                removeComments: true,
                removeRedundantAttributes: true,
                removeScriptTypeAttributes: true,
                removeStyleLinkTypeAttributes: true,
                useShortDoctype: true,
            },
        }),
    ],
    node: {fs: 'empty'},
    resolve: {
        alias: {
            vue: 'vue/dist/vue.js'
        }
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [
                    'style-loader',
                    'css-loader',
                ],
            },
            {
                test: /\.(png|svg|jpg|gif)$/,
                loader: 'file-loader',
                options: {
                    name: '[name].[ext]',
                    esModule: false,
                },
            },
            {
                test: /\.vue$/,
                loader: 'vue-loader'
            },
            {
                test: /\.(woff|woff2|eot|ttf)$/,
                loader: 'url-loader?limit=100000',
            },
        ],
    },
};
